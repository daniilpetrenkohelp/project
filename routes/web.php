<?php

use App\Http\Controllers\Api\ContactsResource;
use App\Http\Controllers\Api\ContractsResource;
use App\Http\Controllers\Api\SupplierContactsResource;
use App\Http\Controllers\Api\SuppliersResource;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\SignupController;
use App\Http\Controllers\PagesController;
use App\Http\Controllers\Workflow\ContractsController;
use App\Http\Controllers\Workflow\SuppliersController;
use App\Http\Controllers\Workflow\TestController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Route;
use Stancl\Tenancy\Middleware\InitializeTenancyByDomain;
use Stancl\Tenancy\Middleware\PreventAccessFromCentralDomains;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**
 * Routes of the public incomingjet website.
 */
foreach (config('tenancy.exempt_domains', []) as $domain) {
    Route::middleware('web')
        ->domain($domain)
        ->group(function () {

            Route::get('/', [SignupController::class, 'showSignupForm'])
                ->name('signup');

            Route::post('registrations', [SignupController::class, 'createRegistrationRequest'])
                ->name('registration.create');
            Route::get('registrations', [SignupController::class, 'waitingForConfirmation'])
                ->name('registration.pending');
            Route::get('registrations/{registrationCode}/confirmation', [SignupController::class, 'confirmRegistrationRequest'])
                ->name('registration.confirmation');

            Route::get('/pages/terms', [PagesController::class, 'terms'])
                ->name('pages.terms');
            Route::get('/pages/gdpr', [PagesController::class, 'gdpr'])
                ->name('pages.gdpr');
            Route::get('/pages/imprint', [PagesController::class, 'imprint'])
                ->name('pages.imprint');
        });
}

/**
 * Tenant specific routes. *.incomingjet.com
 */
Route::middleware([
    'web',
    InitializeTenancyByDomain::class,
    PreventAccessFromCentralDomains::class,
])->group(function () {

// UI Authentication and Logout

    Route::get('login', [LoginController::class, 'showLoginForm'])
        ->name('login')
        ->middleware('guest');

    Route::post('login', [LoginController::class, 'login'])
        ->name('login.attempt')
        ->middleware('guest');

    Route::post('logout', [LoginController::class, 'logout'])
        ->name('logout');

// UI Contracts

    Route::get('/', [ContractsController::class, 'index'])
        ->name('components.contracts')
        ->middleware('auth');
    Route::get('/contracts/create', [ContractsController::class, 'create'])
        ->name('components.contracts.create')
        ->middleware('auth');
    Route::get('/contracts/{uuid}', [ContractsController::class, 'edit'])
        ->name('components.contracts.edit')
        ->middleware('auth');
    Route::post('/contracts', [ContractsController::class, 'store'])
        ->name('components.contracts.store')
        ->middleware('auth');
    Route::put('/contracts', [ContractsController::class, 'update'])
        ->name('components.contracts.update')
        ->middleware('auth');

// UI Suppliers

    Route::get('/suppliers', [SuppliersController::class, 'index'])
        ->name('components.suppliers')
        ->middleware('auth');

// API SupplierContacts Resource

    Route::get('/api/v1/suppliers/{supplierUUID}/contacts', [SupplierContactsResource::class, 'list'])
        ->name('api.supplier-contacts.list')
        ->middleware('auth');
    Route::post('/api/v1/supplier-contacts/{uuid}', [SupplierContactsResource::class, 'create'])
        ->name('api.supplier-contacts.create')
        ->middleware('auth');
    Route::get('/api/v1/supplier-contacts/{uuid}', [SupplierContactsResource::class, 'read'])
        ->name('api.supplier-contacts.read')
        ->middleware('auth');

// API Contacts Resource

    Route::get('/api/v1/contacts', [ContactsResource::class, 'list'])
        ->name('api.contacts.list')
        ->middleware('auth');
    Route::post('/api/v1/contacts', [ContactsResource::class, 'create'])
        ->name('api.contacts.create')
        ->middleware('auth');
    Route::get('/api/v1/contacts/{uuid}', [ContactsResource::class, 'read'])
        ->name('api.contacts.read')
        ->middleware('auth');
    Route::put('/api/v1/contacts/{uuid}', [ContactsResource::class, 'update'])
        ->name('api.contacts.update')
        ->middleware('auth');

// API Suppliers Resource

    Route::get('/api/v1/suppliers', [SuppliersResource::class, 'list'])
        ->name('api.suppliers.list')
        ->middleware('auth');
    Route::get('/api/v1/suppliers/{uuid}', [SuppliersResource::class, 'read'])
        ->name('api.suppliers.read')
        ->middleware('auth');
    Route::post('/api/v1/suppliers', [SuppliersResource::class, 'create'])
        ->name('api.suppliers.create')
        ->middleware('auth');
    Route::put('/api/v1/suppliers/{uuid}', [SuppliersResource::class, 'update'])
        ->name('api.suppliers.update')
        ->middleware('auth');
    Route::delete('/api/v1/suppliers/{uuid}', [SuppliersResource::class, 'delete'])
        ->name('api.suppliers.delete')
        ->middleware('auth');

// API PDF Generation
    Route::get('/api/v1/contracts/{uuid}/pdf', [ContractsResource::class, 'exportPDF'])
        ->name('api.contracts.export.pdf')
        ->middleware('auth');



// Experiment: Search

    Route::get('test/search', [TestController::class, 'search'])
        ->name('test.search')
        ->middleware('remember', 'auth');

// 500 error

    Route::get('500', function () {
        // Force debug mode for this endpoint in the demo environment
        if (App::environment('demo')) {
            Config::set('app.debug', true);
        }
    });

});

