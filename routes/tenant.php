<?php

declare(strict_types=1);

use App\Http\Controllers\Api\ContractsResource;
use Illuminate\Support\Facades\Route;
use Stancl\Tenancy\Middleware\InitializeTenancyByDomain;
use Stancl\Tenancy\Middleware\PreventAccessFromCentralDomains;


Route::middleware([
    'auth:api',
    InitializeTenancyByDomain::class,
    PreventAccessFromCentralDomains::class,
])->prefix('api/v1/')->group(function () {

    Route::get('contracts', [ContractsResource::class, 'list'])
        ->name('api.contracts');
    Route::post('contracts', [ContractsResource::class, 'create'])
        ->name('api.contracts.create');
    Route::post('contacts/{uuid}', [ContractsResource::class, 'edit'])
        ->name('api.contracts.edit');

});


///*
//|--------------------------------------------------------------------------
//| Tenant Routes
//|--------------------------------------------------------------------------
//|
//| Here you can register the tenant routes for your application.
//| These routes are loaded by the TenantRouteServiceProvider.
//|
//| Feel free to customize them however you want. Good luck!
//|
//*/
//
//Route::middleware([
//    'web',
//    InitializeTenancyByDomain::class,
//    PreventAccessFromCentralDomains::class,
//])->group(function () {
//    Route::get('/', function () {
//        return 'This is your multi-tenant application. The id of the current tenant is ' . tenant('id');
//    });
//});
