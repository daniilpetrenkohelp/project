<?php

namespace App\Providers;

use App\Domain\Contacts\Models\Entities\Contact;
use App\Domain\Contacts\Models\Entities\ContactDetail;
use App\Domain\Core\Models\BaseModel;
use App\Domain\Core\Models\Entities\IJEntity;
use App\Observers\IJEntityObserver;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        Factory::guessFactoryNamesUsing(function (string $modelName) {
            return 'Database\\Factories\\' . class_basename($modelName) . 'Factory';
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
