<?php

namespace App\Providers;

use App\Domain\Contacts\Models\Entities\Contact;
use App\Domain\Contacts\Models\Entities\ContactDetail;
use App\Domain\Contacts\Models\Entities\Location;
use App\Domain\Contacts\Models\Entities\SupplierContact;
use App\Domain\Core\Models\Supplier;
use App\Observers\IJEntityObserver;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Log;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        Log::debug('enter EventServiceProvider::boot');

        $modelClasses = [
            Location::class,
            Supplier::class,
            SupplierContact::class,
            Contact::class,
            ContactDetail::class,
        ];

        foreach($modelClasses as &$modelClass) {
            $modelClass::observe(IJEntityObserver::class);
        }
    }

}
