<?php


namespace App\Helpers\Http;


trait IJResource
{

    /**
     * @return mixed
     */
    private function respondWithJSON(mixed $jsonResponse, int $httpResponseCode) : mixed
    {
        $asFormated = request()->wantsJson() ? 0 : JSON_PRETTY_PRINT;
        return response()->json($jsonResponse, $httpResponseCode, [], $asFormated);
    }

}
