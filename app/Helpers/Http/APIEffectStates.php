<?php

namespace App\Helpers\Http;

class APIEffectStates
{
    const OUTCOME_STATE_UNCHANGED = 'unchanged';
    const OUTCOME_STATE_DELETED = 'deleted';
    const OUTCOME_STATE_NOT_FOUND = 'not found';
}
