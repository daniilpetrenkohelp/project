<?php

namespace App\Mail;

use App\Domain\Core\Models\Tenant;
use App\Domain\Core\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class InitialUserCreatedMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $registrationCode;
    /**
     * @var Tenant
     */
    private $tenant;
    /**
     * @var User
     */
    private $user;
    /**
     * @var string
     */
    private $initialPassword;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(
        Tenant $tenant,
        User $user,
        string $initialPassword)
    {
        $this->tenant = $tenant;
        $this->user = $user;
        $this->initialPassword = $initialPassword;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        Log::debug($this->tenant);
        Log::debug($this->tenant->domains()->first());
        $tenantUrl = $this->tenant->domains()->first()->domain;
        $tenantUrl = "https://${tenantUrl}";

        $mailSender = env('MAIL_FROM_ADDRESS');
        return $this->from($mailSender)
            ->view('emails.environment-ready')
            ->with([
                'tenantUrl' => $tenantUrl,
                'login' => $this->user->email,
                'password' => $this->initialPassword,
            ]);
    }

}
