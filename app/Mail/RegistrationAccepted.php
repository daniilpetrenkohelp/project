<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RegistrationAccepted extends Mailable
{
    use Queueable, SerializesModels;

    protected $registrationCode;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(string $registrationCode, string $confirmationURL)
    {
        $this->registrationCode = $registrationCode;
        $this->confirmationURL = $confirmationURL;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $mailSender = env('MAIL_FROM_ADDRESS');
        return $this->from($mailSender)
            ->view('emails.registration')
            ->subject('Finish your registration')
            ->with([
                'registrationCode' => $this->registrationCode,
                'confirmationURL' => $this->confirmationURL,
            ]);
    }

}
