<?php


namespace App\Domain\Contacts\Services;


use App\Domain\Contacts\Models\Entities\Location;
use App\Exceptions\DomainObjectInvalidException;

class LocationService
{

    /**
     * @param string $locationID
     * @return Location
     */
    public function readById(string $locationID) : Location {
        return Location::where('id', $locationID)->firstOrFail();
    }

    /**
     * @param string $locationUUID
     * @return Location
     */
    public function readByUUID(string $locationUUID) : Location {
        return Location::where('id', $locationUUID)->firstOrFail();
    }

    /**
     * @param array $addressDTO
     * @return Location
     * @throws DomainObjectInvalidException
     */
    public function createByAddress(array $addressDTO) : Location {
        $requiredFields = [
            Location::FIELD_ADDRESSLINE_FIRST,
            Location::FIELD_ZIPCODE,
            Location::FIELD_CITY,
        ];

        $allFieldsGiven = true;
        foreach($requiredFields as &$requiredField) {
            $allFieldsGiven = $allFieldsGiven && array_key_exists($requiredField, $addressDTO);
        }

        if(!$allFieldsGiven) {
            $message = "Cannot create Location. Minimal field requirements for address locations not met.";
            $fields = json_encode($requiredFields, true);
            $message .= " ${fields}";
            throw new DomainObjectInvalidException($message);
        }

        return Location::create($addressDTO);
    }

}
