<?php

namespace App\Domain\Contacts\Models\Entities;

use App\Domain\Contacts\Models\Entities\Contact;
use App\Domain\Core\Models\BaseModel;
use App\Domain\Core\Models\Entities\IJEntity;
use Faker\Provider\Base;
use Illuminate\Database\Eloquent\Concerns\HasEvents;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;

class ContactDetail extends IJEntity
{
    use SoftDeletes;
    use HasFactory;

    const TYPE_EMAIL = 'email';
    const TYPE_MOBILE = 'mobile';
    const TYPE_PHONE = 'phone';
    const TYPE_FAX = 'fax';
    const TYPE_FACEBOOK = 'facebook';
    const TYPE_WEBSITE = 'website';

    const ALL_CHANNEL_TYPES = [
        ContactDetail::TYPE_PHONE,
        ContactDetail::TYPE_MOBILE,
        ContactDetail::TYPE_EMAIL,
        ContactDetail::TYPE_FAX,
        ContactDetail::TYPE_WEBSITE,
        ContactDetail::TYPE_FACEBOOK,
    ];

    public function contact()
    {
        return $this->belongsTo(Contact::class);
    }

    public function getNameAttribute()
    {
        return $this->first_name .' '. $this->last_name;
    }

}
