<?php

namespace App\Domain\Contacts\Models\Entities;

use App\Domain\Core\Models\Entities\IJEntity;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;

class Location extends IJEntity
{
    use SoftDeletes;
    use HasFactory;

    const FIELD_ADDRESSLINE_FIRST = 'addressline_first';
    const FIELD_ADDRESSLINE_SECOND = 'addressline_second';
    const FIELD_ADDRESSLINE_THIRD = 'addressline_third';
    const FIELD_ZIPCODE = 'zipcode';
    const FIELD_PROVINCE = 'province';
    const FIELD_COUNTRY = 'country';
    const FIELD_CITY = 'city';

    const FIELD_GPS_LATITUDE = 'gps_lat';
    const FIELD_GPS_LONGITUDE = 'gps_long';
}
