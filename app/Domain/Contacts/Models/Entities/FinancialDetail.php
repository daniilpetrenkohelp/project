<?php

namespace App\Domain\Contacts\Models\Entities;

use App\Domain\Contacts\Models\Entities\Contact;
use App\Domain\Core\Models\BaseModel;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;

class FinancialDetail extends BaseModel
{
    use SoftDeletes;
    use HasFactory;

    const INVOICE_FORMAT_ITALYMAIL = 'italymail';
    const INVOICE_FORMAT_PDF = 'pdf';
    const INVOICE_FORMAT_EMAIL = 'email';
    const INVOICE_FORMAT_PAPER = 'paper';

    public function contact()
    {
        return $this->belongsTo(Contact::class);
    }

    public function getNameAttribute()
    {
//        return $this->first_name.' '.$this->last_name;
    }

    public function scopeOrderByName($query)
    {
//        $query->orderBy('last_name')->orderBy('first_name');
    }

    public function scopeFilter($query, array $filters)
    {
//        $query->when($filters['search'] ?? null, function ($query, $search) {
//            $query->where(function ($query) use ($search) {
//                $query->where('first_name', 'like', '%'.$search.'%')
//                    ->orWhere('last_name', 'like', '%'.$search.'%')
//                    ->orWhere('email', 'like', '%'.$search.'%')
//                    ->orWhereHas('organization', function ($query) use ($search) {
//                        $query->where('name', 'like', '%'.$search.'%');
//                    });
//            });
//        })->when($filters['trashed'] ?? null, function ($query, $trashed) {
//            if ($trashed === 'with') {
//                $query->withTrashed();
//            } elseif ($trashed === 'only') {
//                $query->onlyTrashed();
//            }
//        });
    }
}
