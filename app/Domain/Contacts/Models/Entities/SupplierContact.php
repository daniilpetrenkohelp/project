<?php

namespace App\Domain\Contacts\Models\Entities;

use App\Domain\Contacts\Models\Entities\Contact;
use App\Domain\Core\Models\BaseModel;
use App\Domain\Core\Models\Entities\IJEntity;
use App\Domain\Core\Models\Supplier;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;

class SupplierContact extends IJEntity
{
    use SoftDeletes;
    use HasFactory;

    const ROLE_SERVICE_LOCATION = 'service_location';
    const ROLE_OPERATING_COMPANY = 'operating_company';
    const ROLE_FRONTDESK = 'frontdesk';
    const ROLE_INFORMATION = 'information';
    const ROLE_OTHER = 'other';
    const ROLE_PRIMARY_CONTACT = 'primary_contact';

    protected $casts = [
        'roles' => 'json',
    ];

    const ALL_CONTRACTABLE_ROLES = [
        // primary, contract relavant contact roles
        SupplierContact::ROLE_SERVICE_LOCATION,
        SupplierContact::ROLE_OPERATING_COMPANY,
        SupplierContact::ROLE_PRIMARY_CONTACT,
    ];

    const ALL_GUEST_SERVICE_ROLES = [
        SupplierContact::ROLE_FRONTDESK,
        SupplierContact::ROLE_INFORMATION,
        SupplierContact::ROLE_OTHER,
    ];

    const ALL_ROLES = [
        // primary, contract relavant contact roles
        SupplierContact::ROLE_SERVICE_LOCATION,
        SupplierContact::ROLE_OPERATING_COMPANY,
        SupplierContact::ROLE_PRIMARY_CONTACT,

        // contacts to services
        SupplierContact::ROLE_FRONTDESK,
        SupplierContact::ROLE_INFORMATION,
        SupplierContact::ROLE_OTHER,
    ];

    public function contact()
    {
        return $this->belongsTo(Contact::class);
    }

    public function supplier()
    {
        return $this->belongsTo(Supplier::class);
    }

}
