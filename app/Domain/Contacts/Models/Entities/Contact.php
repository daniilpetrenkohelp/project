<?php

namespace App\Domain\Contacts\Models\Entities;

use App\Domain\Core\Models\BaseModel;
use App\Domain\Core\Models\Entities\IJEntity;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Scout\Searchable;

class Contact extends IJEntity
{
    use SoftDeletes;
//    use Searchable;
    use HasFactory;

    const TYPE_PERSON = 'person';
    const TYPE_COMPANY = 'company';
    const TYPE_ORGANIZATION = 'organization';

    public function financialDetails()
    {
        return $this->hasMany(FinancialDetail::class);
    }

    public function contactDetails()
    {
        return $this->hasMany(ContactDetail::class);
    }

    public function locations()
    {
        return $this->hasMany(Location::class);
    }

}
