<?php

namespace App\Domain\Contacts\Models;

use App\Domain\Contacts\Models\ContactDetail;
use App\Domain\Contacts\Models\FinancialDetail;
use App\Domain\Contacts\Models\Location;
use App\Domain\Core\Models\Supplier;

class SupplierRelationsAggregate
{

    public function __construct(
        Supplier $supplier,
        array $supplierContacts
    ) {
        $this->supplier = $supplier;
        $this->contacts = $supplierContacts;
    }

    public function toJson() {
        $supplierJson = $this->supplier->toArray();
        return $supplierJson;
    }

}
