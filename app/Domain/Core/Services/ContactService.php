<?php

namespace App\Domain\Core\Services;

use App\Domain\Contacts\Models\Entities\Contact;
use App\Domain\Contacts\Models\Entities\SupplierContact;
use App\Exceptions\DomainObjectNotFoundException;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ContactService
{
    /**
     * Reads all Contatcs, that are allowed to close contracts with incoming agency.
     * Includes relations to Supplier and Contact Entities.
     *
     */
    public function readContractPartnerContacts(): Collection
    {
        $oneOrMoreExpectedRoles = array_map(function(string $role) {
            return '"' . $role . '"';
        }, SupplierContact::ALL_CONTRACTABLE_ROLES);
        $oneOrMoreExpectedRoles = implode(",", $oneOrMoreExpectedRoles);

        $contractAddresses = DB::table('contacts')
            ->select(
                'suppliers.name as supplier_name',
                'contacts.name',
                'supplier_contacts.contact_id as contact_id',
                'supplier_contacts.supplier_id as supplier_id',
                'supplier_contacts.roles as roles',
                'supplier_contacts.uuid as uuid',
            )
            ->join('supplier_contacts', 'contacts.id', '=', 'supplier_contacts.contact_id')
            ->join('suppliers', 'suppliers.id', '=', 'supplier_contacts.supplier_id')
            ->whereRaw("JSON_OVERLAPS(supplier_contacts.roles, JSON_ARRAY(${oneOrMoreExpectedRoles}))")
            ->get();

        $contractPartners = SupplierContact::hydrate($contractAddresses->toArray());
        return $contractPartners;
    }

    public function readSupplierRole(string $uuid)
    {
        return SupplierContact::where('uuid', $uuid)->firstOrFail();
    }

    public function readSupplierOfSupplierContactUUID($supplierUUID)
    {
        Log::debug("enter ContactService->readSupplierOfSupplierContactUUID($supplierUUID)");
        $supplierContact = SupplierContact::where('uuid', $supplierUUID)->firstOrFail();
        return $supplierContact->supplier()->get()->first();
    }

    public function createContactByConfig(array $contactDto)
    {
        return Contact::create($contactDto);
    }

    /**
     * @param string $contactUUID
     * @return Contact
     * @throws DomainObjectNotFoundException
     */
    public function readContactByUUID(string $contactUUID) : Contact
    {
        try {
            return Contact::where('uuid', $contactUUID)->firstOrFail();
        } catch (\Exception $error) {
            throw new DomainObjectNotFoundException("Could not read Contact:${contactUUID}.");
        }
    }

}
