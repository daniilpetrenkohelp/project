<?php


namespace App\Domain\Core\Services;

use App\Domain\Core\Models\Account;
use App\Domain\Core\Models\Country;
use App\Domain\Core\Models\Currency;
use App\Domain\Core\Models\Language;
use App\Domain\Core\Models\Organization;
use App\Domain\Core\Models\Tenant;
use App\Domain\Core\Models\User;
use App\Mail\InitialUserCreatedMail;
use App\Mail\RegistrationAccepted;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use League\Flysystem\FileNotFoundException;

class TenantSetupService
{
    public function createInitialUser(Tenant $tenant) : User {
        $user = null;

        tenancy()->runForMultiple([$tenant->id], function ($tenant) use(&$user) {
            // TODO: make passwords random
            $registrationEmail = $tenant->registration_email;
            $randomPassword = Str::random();
            $apiToken = Str::random();

            // @TODO: centralize this toggle into global configuraiton
            if(env("IJ_ACT_TESTABLE", false) === true) {
                $randomPassword = env('IJ_TESTING_PASSWORD');
                $apiToken = env('IJ_TESTING_API_TOKEN');
                $apiToken = "${registrationEmail}-${apiToken}";
            }

            $user = User::create([
                'email' => $registrationEmail,
                'password' => $randomPassword,
                'first_name' => '',
                'last_name' => '',
                'api_token' => $apiToken,
            ]);

            // TODO: setup queue system.
            // TODO: extract mail sending to external event handle. throw new user registered event instead.
            $mailable = new InitialUserCreatedMail(
                $tenant,
                $user,
                $randomPassword,
            );

            Mail::to($registrationEmail)->send($mailable);
            return $user;
        });

        return $user;
    }

    public function setupTableStructures(Tenant $tenant) {
        Artisan::call('tenants:migrate', [
            '--tenants' => [ $tenant->id ],
        ]);
    }

    public function populateWithRandomData(Tenant $tenant)
    {
        Artisan::call('tenants:seed', [
            '--class' => 'DatabaseSeeder',
            '--tenants' => [ $tenant->id ],
        ]);
    }

    public function updateModelIndicies(Tenant $_tenant) {
        $searchableModels = [
//            Organization::class,
        ];

        tenancy()->runForMultiple([$_tenant->id], function ($tenant) use ($searchableModels) {
            $tenantId = $tenant->id;
            foreach($searchableModels as $searchable) {
//                $model = new $searchable();
//                $indexName = $model->searchableAs();
//                echo "${indexName}\n";

                Artisan::call('tenants:run', [
                    '--tenants' => [ $tenantId ],
                    '--argument' => [
                        "model=${searchable}",
                    ],
                    'commandname' => 'scout:import',
                ]);

            }
        });
    }

    public function populateTenantSystemTables(Tenant $tenant)
    {
        tenancy()->runForMultiple([$tenant->id], function() {
            $this->populateTenantLanguagesTable();
            $this->populateTenantCountriesTable();
            $this->populateCurrenciesTable();
            // TODO: populate currency, country and locales table
        });
    }

    private function populateTenantLanguagesTable()
    {
        $json = Storage::get('data/iso-languages.json');
        $languages = json_decode($json);
        foreach($languages as $isoCode => $language) {
            Language::create(array(
                'iso_code' => $isoCode,
                'native_name' => $language->nativeName,
                'name' => $language->name,
            ));
        }
    }

    private function populateTenantCountriesTable()
    {
        $json = Storage::get('data/countries.json');
        $countries = json_decode($json, true);

        foreach($countries as &$country) {
            unset($country['createdAt']);
            unset($country['updatedAt']);
            unset($country['parent_tr_iso_nr']);
        }

        Country::insert($countries);
    }

    private function populateCurrenciesTable()
    {
        $json = Storage::get('data/currencies.json');
        $currencies = json_decode($json, true);

        foreach($currencies as $currencyCode => $currencyNameEn) {
            Currency::create([
                'currency_code' => $currencyCode,
                'currency_name_en' => $currencyNameEn,
            ]);
        }
    }

}
