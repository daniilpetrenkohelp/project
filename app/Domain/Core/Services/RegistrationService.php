<?php


namespace App\Domain\Core\Services;

use App\Domain\Core\Models\Tenant;
use App\Exceptions\RegistrationAlreadyConfirmedError;
use App\Exceptions\RegistrationCodeUnkownError;
use App\Exceptions\RegistrationEmailAlreadyKnownError;
use App\Mail\RegistrationAccepted;
use Carbon\Carbon;
use http\Exception\RuntimeException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class RegistrationService
{
    /**
     * @var TenantSetupService
     */
    private $tenantSetupService;

    public function __construct(TenantSetupService $_tenantSetupService) {
        $this->tenantSetupService = $_tenantSetupService;
    }

    public function setupByMailAndProvision(string $registrantEmail) {
        $tenant = $this->prepareTenantRegistration($registrantEmail);
        $tenant = $this->confirmTenantRegistration($tenant->registration_code);
        $this->setupTenantEnvironment($tenant);
        $this->tenantSetupService->setupTableStructures($tenant);
        $this->tenantSetupService->createInitialUser($tenant);
        $this->tenantSetupService->populateWithRandomData($tenant);
        $this->tenantSetupService->updateModelIndicies($tenant);
        return $tenant;
    }

    public function setupByMailAndProvisionMinimal(string $registrantEmail) {
        $tenant = $this->prepareTenantRegistration($registrantEmail);
        $tenant = $this->confirmTenantRegistration($tenant->registration_code);
        $this->setupTenantEnvironment($tenant);
        $this->tenantSetupService->setupTableStructures($tenant);
        $this->tenantSetupService->createInitialUser($tenant);
//        $this->tenantSetupService->populateWithRandomData($tenant);
//        $this->tenantSetupService->updateModelIndicies($tenant);
        return $tenant;
    }
        /**
     * Pre-registers a new tenant given by his email address.
     * Will also generate a random subdomain domain for later use.
     * Actively sends a registration confirnation request via email.
     *
     * @param string $registrationEmail must be unique within our database
     * @throws RegistrationEmailAlreadyKnownError
     */
    public function prepareTenantRegistration(string $registrationEmail) : Tenant {
        Log::debug('enter prepareTenantRegistration: ' . $registrationEmail);

        $tenantId = uniqid();
        if(env("IJ_ACT_TESTABLE", false) === true) {
            $tenantId = env('IJ_TESTING_TENANT_FIX_ID');
        }

        $registrationCode = uniqid();
        $tenantFQDN = $this->generateTenantFQDN();
        $tenant = null;

        try {
            $tenant = Tenant::create([
                Tenant::FIELD_ID => $tenantId,
                Tenant::JSON_TENANCY_DB_NAME => "tenant_${tenantId}",
                Tenant::FIELD_REGISTRATION_EMAIL => $registrationEmail,
                Tenant::FIELD_REGISTRATION_CODE => $registrationCode,
            ]);
            $tenant->domains()->create([
                'domain'  => $tenantFQDN,
            ]);
            $confirmationURL = route('registration.confirmation', $tenant->registration_code);

            // TODO: setup queue system.
            // TODO: extract mail sending to external event handle. throw new user registered event instead.
            $mailable = new RegistrationAccepted(
                $tenant->registration_code,
                $confirmationURL,
            );

            Log::debug("Sending to ");
            Log::debug($registrationEmail);
            Log::debug("Mailable ");
            Log::debug(print_r($mailable, true));

            Mail::to($registrationEmail)
                ->send($mailable);
        } catch (QueryException $error) {
            $DUPLICATE_ENTRY_ERROR = 23000;
            $errorCode = $error->getCode();

            if($errorCode == $DUPLICATE_ENTRY_ERROR) {
                throw new RegistrationEmailAlreadyKnownError("The registrant '${registrationEmail}' already exists");
            }

            throw $error;
        }

        return $tenant;
    }

    /**
     * Call to confirm a pre registration of a new tenant/user.
     *
     * @param string $registrationCode must match a given registration_code from the database.
     * @return mixed
     * @throws RegistrationAlreadyConfirmedError
     * @throws RegistrationCodeUnkownError
     */
    public function confirmTenantRegistration(string $registrationCode): Tenant {
        $result = null;
        try {
            $tenant = Tenant::where(Tenant::FIELD_REGISTRATION_CODE, $registrationCode)->firstOrFail();
            if($tenant->confirmed_at != null) {
                throw new RegistrationAlreadyConfirmedError();
            }
            $tenant->confirmed_at = Carbon::now();
            if($tenant->save()) {
                $result = $tenant;
            } else {
                $message = "Unknown error while confirming code '${registrationCode}'.";
                throw new RuntimeException($message);
            }
        } catch (ModelNotFoundException $error) {
            throw new RegistrationCodeUnkownError("The given registration code '${registrationCode}' is unknown");
        }

        return $result;
    }

    /**
     * Sets up database, search indecies etc. for the given tenant.
     *
     * @param Tenant $tenant the tenant to create the environment for
     */
    public function setupTenantEnvironment(Tenant $tenant) {
        $this->tenantSetupService->populateTenantSystemTables($tenant);
    }

    /**
     * Genereates a random name from a vocabulary.
     *
     * @return string a string like name1-name2-randnumber
     */
    private function generateRandomName() {
        $separator = '-';

        // TODO: would be nice to use travel keywords
        $adjectives = [
            "autumn", "hidden", "bitter", "misty", "silent", "empty", "dry", "dark",
            "summer", "icy", "delicate", "quiet", "white", "cool", "spring", "winter",
            "patient", "twilight", "dawn", "crimson", "wispy", "weathered", "blue",
            "billowing", "broken", "cold", "damp", "falling", "frosty", "green",
            "long", "late", "lingering", "bold", "little", "morning", "muddy", "old",
            "red", "rough", "still", "small", "sparkling", "throbbing", "shy",
            "wandering", "withered", "wild", "black", "young", "holy", "solitary",
            "fragrant", "aged", "snowy", "proud", "floral", "restless", "divine",
            "polished", "ancient", "purple", "lively", "nameless"
        ];

        $nouns = [
            "waterfall", "river", "breeze", "moon", "rain", "wind", "sea", "morning",
            "snow", "lake", "sunset", "pine", "shadow", "leaf", "dawn", "glitter",
            "forest", "hill", "cloud", "meadow", "sun", "glade", "bird", "brook",
            "butterfly", "bush", "dew", "dust", "field", "fire", "flower", "firefly",
            "feather", "grass", "haze", "mountain", "night", "pond", "darkness",
            "snowflake", "silence", "sound", "sky", "shape", "surf", "thunder",
            "violet", "water", "wildflower", "wave", "water", "resonance", "sun",
            "wood", "dream", "cherry", "tree", "fog", "frost", "voice", "paper",
            "frog", "smoke", "star"
        ];

        // TODO: set seed for better random numbers
        $num  = intval(rand(2, 3) * pow(5,5));
        $adj  = $adjectives[array_rand($adjectives, 1)];
        $noun = $nouns[array_rand($nouns, 1)];

        return $adj.$separator.$noun.$separator.$num;
    }

    /**
     * Generates a random full qualified domain name based on a dictionary and the environment's application url.
     *
     * @return array
     */
    private function generateTenantFQDN(): string
    {
        $url = env("APP_URL");
        Log::debug("generateTenantFQDN APP_URL=${url}");

        $urlComponents = parse_url($url);
        $tmp = explode('.', $urlComponents['host']);
        $tld = array_pop($tmp);
        $hostname = array_pop($tmp);
        $subdomain = $this->generateRandomName();
        $tenantHostname = "${subdomain}.${hostname}.${tld}";

        // @TODO: centralize this toggle into global configuraiton
        if(env("IJ_ACT_TESTABLE", false) === true) {
            $tenantHostname = env('IJ_TESTING_TENANT_FQDN');
        }


        return $tenantHostname;
    }

}
