<?php

namespace App\Domain\Core\Services;


use App\Domain\Contacts\Models\Entities\ContactDetail;
use App\Domain\Contacts\Models\Entities\SupplierContact;
use App\Domain\Suppliers\Services\SupplierService;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class SupplierContactService
{

    private SupplierService $supplierService;
    private ContactService $contactService;
    private ContactDetailService $contactDetailsService;

    /**
     * @param ContactService $contactService
     * @param SupplierService $supplierService
     * @param ContactDetailService $contactDetailsService
     */
    public function __construct(
        ContactService       $contactService,
        SupplierService      $supplierService,
        ContactDetailService $contactDetailsService,
    )
    {
        $this->supplierService = $supplierService;
        $this->contactService = $contactService;
        $this->contactDetailsService = $contactDetailsService;
    }

    /**
     * @param array $supplierContactRequestDTO
     * @throws \App\Exceptions\DomainObjectNotFoundException
     */
    public function createContactAtSupplier(array $supplierContactRequestDTO)
    {
        $supplierUUID = $supplierContactRequestDTO['supplier_uuid'];
        $supplier = $this->supplierService->readSupplierByUUID($supplierUUID);

        DB::beginTransaction();
        try {

            $beAssociatedWithExistingContact = isset($supplierContactRequestDTO['contact_uuid']);
            if ($beAssociatedWithExistingContact === true) {
                $contactUUID = $supplierContactRequestDTO['contact_uuid'];
                $contactEntity = $this->contactService->readContactByUUID($contactUUID);
            } else {
                $createNewContactConfig = [
                    'name' => $supplierContactRequestDTO['name'],
                    'type' => $supplierContactRequestDTO['type'],
                ];

                // TODO: create subchannels/contact possibilities and locations
                $contactEntity = $this->contactService->createContactByConfig($createNewContactConfig);
            }

            // TODO: create additional channels or just channels first time
            $contactChannelEntities = $this->contactDetailsService->createMultipleContactChannelsAtContact($contactEntity, $supplierContactRequestDTO['channels']);

            // TODO: check roles for validity/model-integrity
            $supplierContactRequestDTO = array_merge($supplierContactRequestDTO, [
                'supplier_id' => $supplier->id,
                'contact_id' => $contactEntity->id,
                'roles' => $supplierContactRequestDTO['roles'],
                'channels' => $contactChannelEntities,
            ]);
            unset($supplierContactRequestDTO['channels']);
            unset($supplierContactRequestDTO['supplier_uuid']);
            unset($supplierContactRequestDTO['name']);
            unset($supplierContactRequestDTO['type']);

            $supplierContact = SupplierContact::create($supplierContactRequestDTO);

            DB::commit();
        } catch (\Exception $error) {
            DB::rollBack();
            throw $error;
        }

        return $supplierContact;
    }

}
