<?php


namespace App\Domain\Core\Services;


use App\Domain\Core\Models\Tenant;

class TenantService
{
    public function findById(string $tenantId) : Tenant {
        return Tenant::where('id', $tenantId)->firstOrFail();
    }

    public function findByRegistrationEmail(string $tenantEmail) : Tenant {
        return Tenant::where('registration_email', $tenantEmail)->firstOrFail();
    }

}
