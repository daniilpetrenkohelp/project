<?php

namespace App\Domain\Core\Services;

use App\Domain\Contacts\Models\Entities\Contact;
use App\Domain\Contacts\Models\Entities\SupplierContact;
use App\Exceptions\DomainObjectNotFoundException;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Http;



class PDFGeneratorService
{


	public function generateSellingContract(string $sellingContractUUID, string $fileName){
		if(Storage::cloud()->exists($fileName)){
			return $cachedFile = $this->getPDFFromCache($fileName);
		}else{
			return $cachedFile = $this->generatePDFFile($fileName);
		}
	}
	private function getContractData(){
		///getting data for sending to API
		//right now just for being sure it works
		return $contractSource = file_get_contents('d:/index.html');//getting data

	}
	private function generatePDFFile(string $fileName){

		$contractSource = $this->getContractData(); //getting data to send
		$api = env('WEASYPRINT_API'); //API URL
		$pdfString = Http::withBody($contractSource, 'text/html')->post($api); //getting a PDF line from API
		Storage::cloud()->put($fileName, $pdfString); //putting a PDF into S3

		return $cachedFile = $this->getPDFFromCache($fileName);
	}
	private function getPDFFromCache(string $fileName){
		return Storage::cloud()->download($fileName);
	}

}