<?php

namespace App\Domain\Core\Services;

use App\Domain\Contacts\Models\Entities\Contact;
use App\Domain\Contacts\Models\Entities\ContactDetail;
use App\Exceptions\DomainObjectCreationException;
use Illuminate\Support\Facades\Log;

class ContactDetailService
{

    public function createMultipleContactChannelsAtContact(Contact $contactEntity, array $channels) : array
    {
        $contactChannelEntities = [];
        foreach($channels as &$contactChannelDTO) {
            $contactChannelDTO = array_merge($contactChannelDTO, [
                'contact_id' => $contactEntity->id,
            ]);
            $contactChannelEntities[] = $this->createContactChannel($contactChannelDTO);
        }

        Log::debug('exit ContactDetailService::createMultipleContactChannelsAtContact');
        return $contactChannelEntities;
    }

    private function createContactChannel(array $contactChannelDTO) : ContactDetail
    {
        Log::debug(
            "enter ContactDetailService::createContactChannel() // " .
            print_r($contactChannelDTO, true)
        );

        // TODO: add more precice error/catch cases:
        // - no contact_id given
        // - incomplete required fields
        // - ...
        try {
            $b = ContactDetail::create($contactChannelDTO);
            return $b;
        } catch(\Error $error) {
            $message = "Unkown error during ContactDetail creation: " .json_encode($contactChannelDTO);
            $message .= ' Message: ' . $error->getMessage();
            Log::debug("Message A: ${message}");
            throw new DomainObjectCreationException($message);
        } catch(\Exception $error) {
            $message = "Unkown error during ContactDetail creation: " .json_encode($contactChannelDTO);
            $message .= ' Message: ' . $error->getMessage();
            Log::debug("Message B: ${message}");
            throw new DomainObjectCreationException($message);
        }
    }


}
