<?php

namespace App\Domain\Core\Models;

use App\Domain\Contacts\Models\Entities\Contact;
use App\Domain\Contacts\Models\Entities\Location;
use App\Domain\Contacts\Models\Entities\SupplierContact;
use App\Domain\Core\Models\Entities\IJEntity;
use App\Domain\Sourcing\Models\Contract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;

class Supplier extends IJEntity
{
    use SoftDeletes;
    use HasFactory;

    public function address() : HasOne
    {
        return $this->hasOne(Location::class);
    }

    public function roles() : HasMany
    {
        return $this->hasMany(SupplierContact::class);
    }

    public function contracts() : HasMany
    {
        return $this->hasMany(Contract::class);
    }

    public function primaryContact() : Contact
    {
        $primaryContact = null;
        $primarySupplierRole = $this->hasMany(SupplierContact::class)->where('is_primary', true)->first();

        if($primarySupplierRole !== null) {
            $primaryContact = $primarySupplierRole->contact();
        }

        return $primaryContact;
    }
}
