<?php

namespace App\Domain\Core\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class ApiToken extends BaseModel
{
    use HasFactory;

    const PROVIDER_PASSOLUTION = 'passolution';
}
