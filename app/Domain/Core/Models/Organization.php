<?php

namespace App\Domain\Core\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Scout\Searchable;

class Organization extends BaseModel
{
    use SoftDeletes;
    use Searchable {
        searchableAs as protected baseSearchableAs;
    }
    use HasFactory;

    public function contacts()
    {
        return $this->hasMany(Contact::class);
    }

    public function scopeFilter($query, array $filters)
    {
        $query->when($filters['search'] ?? null, function ($query, $search) {
            $query->where('name', 'like', '%'.$search.'%');
        })->when($filters['trashed'] ?? null, function ($query, $trashed) {
            if ($trashed === 'with') {
                $query->withTrashed();
            } elseif ($trashed === 'only') {
                $query->onlyTrashed();
            }
        });
    }

    public function searchableAs()
    {
        $tenant = tenant();
        $prefix = $tenant->id;
        $tableName = $this->baseSearchableAs();
        return  "${prefix}_${tableName}";
    }
}
