<?php

namespace App\Domain\Core\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\URL;
use League\Glide\Server;
use Laravel\Scout\Searchable;

class Country extends BaseModel
{
    use SoftDeletes;

    protected $fillable = [
        "zone_flag",
        "is_uno_member",
        "internet_tld",
        "shortname_local",
        "shortname_en",
        "shortname_de",
        "calling_code",
        "parent_tr_iso_nr",
        "official_name_local",
        "official_name_en",
        "official_name_de",
        "iso_nr",
        "iso_code",
        "short_id",
        "is_eu_member",
        "currency_iso_number",
        "currency_iso_code",
        "capital",
        "address_format",
    ];

}
