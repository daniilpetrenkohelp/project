<?php

namespace App\Domain\Core\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;

abstract class BaseModel extends Eloquent
{
    protected $guarded = [];

    protected $perPage = 10;

    protected $hidden = [
        'id',
        'deleted_at',
    ];

    public function resolveRouteBinding($value, $field = null)
    {
        return in_array(SoftDeletes::class, class_uses($this))
            ? $this->where($this->getRouteKeyName(), $value)->withTrashed()->first()
            : parent::resolveRouteBinding($value);
    }
}
