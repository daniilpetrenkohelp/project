<?php

namespace App\Domain\Core\Models;

//use Laravel\Scout\Searchable;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Stancl\Tenancy\Database\Models\Domain;
use Stancl\Tenancy\Database\Models\Tenant as BaseTenant;
use Stancl\Tenancy\Contracts\TenantWithDatabase;
use Stancl\Tenancy\Database\Concerns\HasDatabase;
use Stancl\Tenancy\Database\Concerns\HasDomains;

class Tenant extends BaseTenant implements TenantWithDatabase
{
    use HasDatabase, HasDomains;

    const FIELD_REGISTRATION_CODE = 'registration_code';
    const FIELD_ID = 'id';
    const FIELD_REGISTRATION_EMAIL = 'registration_email';
    const FIELD_UPDATED_AT = 'updated_at';
    const FIELD_CREATED_AT = 'created_at';
    const FIELD_CONFIRMED_AT = 'confirmed_at';
    const JSON_TENANCY_DB_NAME = 'tenancy_db_name';

    public static function getCustomColumns(): array
    {
        return [
            self::FIELD_ID,
            self::FIELD_REGISTRATION_EMAIL,
            self::FIELD_REGISTRATION_CODE,
            self::FIELD_CREATED_AT,
            self::FIELD_UPDATED_AT,
            self::FIELD_CONFIRMED_AT,
        ];
    }

    public function domains() : HasMany
    {
        return $this->hasMany(Domain::class);
    }


}
