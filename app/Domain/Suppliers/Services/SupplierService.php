<?php


namespace App\Domain\Suppliers\Services;

use App\Domain\Contacts\Services\LocationService;
use App\Domain\Core\Models\Supplier;
use App\Exceptions\DomainObjectCreationException;
use App\Exceptions\DomainObjectDuplicateException;
use App\Exceptions\DomainObjectInvalidException;
use App\Exceptions\DomainObjectNotDeletedException;
use App\Exceptions\DomainObjectNotFoundException;
use App\Exceptions\RelatingDomainObjectsFailedException;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;


class SupplierService {

    /**
     * @var LocationService
     */
    private LocationService $locationService;

    /**
     * @param LocationService $locationService
     */
    public function __construct(
        LocationService $locationService
    ) {
        $this->locationService = $locationService;
    }

    /**
     * @param string $supplierUUID
     * @return Supplier
     * @throws DomainObjectNotFoundException
     */
    public function readSupplierByUUID(string $supplierUUID) : Supplier {
        $supplierEntity = null;

        try {
            $supplierEntity = Supplier::where('uuid', $supplierUUID)->firstOrFail();
        } catch (\Exception $error) {
            throw new DomainObjectNotFoundException("Could not read Supplier:${supplierUUID}.");
        }

        return $supplierEntity;
    }

    /**
     * @param string $supplierUUID
     * @return Supplier
     * @throws DomainObjectNotFoundException
     * @throws DomainObjectNotDeletedException
     * @throws \Exception
     */
    public function deleteSupplierByUUID(string $supplierUUID) : Supplier
    {
        $supplierEntity = $this->readSupplierByUUID($supplierUUID);

        $isDeleted = $supplierEntity->delete();
        if($isDeleted !== true) {
            throw new DomainObjectNotDeletedException("Could not delete Supplier:${supplierUUID}.");
        }

        return $supplierEntity;
    }

    /**
     * @return mixed
     */
    public function readAllSuppliers() : mixed
    {
        return Supplier::all();
    }

    /**
     * @param array $supplierRequestDto
     * @throws DomainObjectDuplicateException
     * @throws DomainObjectCreationException
     * @return mixed
     */
    public function createByConfig(array $supplierRequestDto)
    {
        $requiredFields = array(
            'name',
        );
        $allFieldsGiven = true;
        foreach($requiredFields as &$requiredField) {
            $allFieldsGiven = $allFieldsGiven && array_key_exists($requiredField, $supplierRequestDto);
        }

        if(!$allFieldsGiven) {
            $message = "Cannot create Supplier. Minimal field requirements for a supplier are not met.";
            $fields = json_encode($requiredFields, true);
            $message .= " ${fields}";
            throw new DomainObjectInvalidException($message);
        }

        try {
            DB::beginTransaction();
            $clonedSupplierRequestDTO = json_decode(json_encode($supplierRequestDto), associative: true);

            $addressDTO = null;
            if(array_key_exists('address', $supplierRequestDto)) {
                $addressDTO = &$clonedSupplierRequestDTO['address'];
            }
            unset($clonedSupplierRequestDTO['address']);

            $supplierEntity = Supplier::create($clonedSupplierRequestDTO);

            if($addressDTO !== null) {
                $locationEntity = $this->locationService->createByAddress($addressDTO);
                $locationEntity->supplier_id = $supplierEntity->id;

                if($locationEntity->save() !== true) {
                    $supplierUUID = $supplierEntity->uuid;
                    $locationUUID = $locationEntity->uuid;
                    $message = "Failed assigning Location:${locationUUID} to Supplier:${supplierUUID}.";
                    throw new RelatingDomainObjectsFailedException($message);
                }
                Log::debug('Related Supplier to Location');
            }

            DB::commit();
        } catch (QueryException $error) {
            DB::rollBack();

            $DUPLICATE_ENTRY_ERROR = 23000;
            $errorCode = $error->getCode();
            if($errorCode == $DUPLICATE_ENTRY_ERROR) {
                $message = "Could not create Supplier. One or more given keys are already existing.";
                throw new DomainObjectDuplicateException($message);
            } else {
                $reason = "Code: " . $error->getCode() . " // " . $error->getMessage();
                $message = "Could not create Supplier: ${reason}";
                throw new DomainObjectCreationException($message);
            }
        } catch(\Exception $error) {
            DB::rollBack();
            throw $error;
        }

        return $supplierEntity;
    }

    public function createContactForSupplier(Supplier $supplier, array $contact) {

    }

}
