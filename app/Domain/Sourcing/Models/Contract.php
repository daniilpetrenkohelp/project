<?php

namespace App\Domain\Sourcing\Models;

use App\Domain\Core\Models\Supplier;
use App\Domain\Core\Models\BaseModel;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contract extends BaseModel
{
    use SoftDeletes;
    use HasFactory;

    const REVENUE_PAYMENT_INVOICE = 'invoice';
    const REVENUE_PAYMENT_OTHER = 'other';
    const PRICING_NET = 'net';
    const PRICING_GROSS = 'gross';

    protected $casts = [
        'document' => 'json',
    ];

    public function supplier()
    {
        return $this->belongsTo(Supplier::class);
    }

    public function products()
    {
//      return $this->hasMany(Product::class);
    }

    public function asContractDocument()
    {
        $contract = $this->document;
        $contract['uuid'] = $this->uuid;
        return $contract;
    }
}
