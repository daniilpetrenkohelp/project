<?php


namespace App\Domain\Sourcing\Services;

use App\Domain\Sourcing\Models\Contract;


class ContractService
{
    public function readContractByUUID(string $uuid) : Contract {
        $contract = Contract::where('uuid', $uuid)->firstOrFail();
        return $contract;
    }

    public function updateContractWithForm(Contract $contract, array $validatedRequest) : Contract {
        $contract->update($validatedRequest);
    }

    public function updateContractHeader(Contract $contract) : Contract {
        die('implement me');
    }

    public function addContactsToContract(Contract $contract, array $contacts) {

    }

}
