<?php

namespace App\Observers;

use App\Domain\Core\Models\Entities\IJEntity;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class IJEntityObserver
{
    /**
     * Handle events after all transactions are committed.
     *
     * @var bool
     */
    public $afterCommit = false;

    /**
     * Handle the IJEntity "created" event.
     *
     * @return void
     */
    public function creating(IJEntity $model)
    {
        Log::debug('IJEntityObserver::creating');
        $user = Auth::user();
        $model->uuid = Str::uuid();
        $model->created_by = $user->id;
        $model->updated_at = null;
    }

    /**
     * @param IJEntity $model
     */
    public function updating(IJEntity $model) {
        Log::debug('IJEntityObserver::updating');
        $user = Auth::user();
        $model->updated_by = $user->id;
        $model->updated_at = Carbon::now();
    }

    /**
     * @param IJEntity $model
     */
    public function deleting(IJEntity $model) {
        Log::debug('IJEntityObserver::deleting');
        $user = Auth::user();
        $model->updated_by = $user->id;
        $model->updated_at = Carbon::now();
    }

}
