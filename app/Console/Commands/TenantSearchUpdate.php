<?php

namespace App\Console\Commands;

use App\Domain\Core\Models\Tenant;
use App\Domain\Core\Services\TenantSetupService;
use Illuminate\Console\Command;

class TenantSearchUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ij:tenant:search:update ${tenantId}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Re/Imports tenant data to search index';
    /**
     * @var TenantSetupService
     */
    private $tenantSetupService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(TenantSetupService $tenantSetupService)
    {
        parent::__construct();
        $this->tenantSetupService = $tenantSetupService;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        /**
         *  php artisan tenants:run scout:import --tenants=t002 --argument="model=App\Models\Product"
         */
        $tenantId = $this->argument('tenantId');
        echo "updating search index of tenant: ${tenantId}\n";
        $tenant = Tenant::where('id', $tenantId)->firstOrFail();
        $this->tenantSetupService->updateModelIndicies($tenant);
        echo "done indexing\n";

        return 0;
    }
}
