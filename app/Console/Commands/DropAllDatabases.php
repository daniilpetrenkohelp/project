<?php

namespace App\Console\Commands;
use Illuminate\Console\Command;

class DropAllDatabases extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ij:testing:db:wipe';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Wipe all databases.';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        // Fetch the defined database name
        $db_type = \Config::get('database.default');
        $connection = \Config::get('database.connections.' . $db_type);
        $host = env('DB_HOST');
        $username = env('DB_USERNAME');
        $password = env('DB_PASSWORD');
        $port = env('DB_PORT');
        $database = env('DB_DATABASE');
        $this->dropDB($host, $username, $password, $database, $port);
    }

    protected function dropDB($host, $username, $password, $database, $port)
    {
        try {
            // Create connection
            $conn = new \mysqli($host, $username, $password, null, $port);
            $this->info(json_encode($conn));

            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }
            // Drop database

            $sql = 'SHOW DATABASES';
            $result = $conn->query($sql);

            $db_names = array();
            while($row = $result->fetch_array(MYSQLI_NUM)) { // for each row of the resultset
                if(str_contains($row[0], "tenant_")) {
                    $db_names[] = $row[0]; // Add db name to $db_names array
                }
            }

            foreach ($db_names as $dbname) {
                $sql = "DROP DATABASE IF EXISTS `${dbname}`";
                if ($conn->query($sql) === TRUE) {
//                    echo "Sucessfully dropped database: ${dbname}\n\r";
                } else {
                    echo "Error dropping database: " . $conn->error;
                }
            }

            $conn->close();
        } catch (Exception $e) {
            $this->info('');
            echo "Error dropping databases";
            $this->info('');
            echo json_encode($e->getMessage());
            $this->info('');
            $this->info('You can try the mysql shell.');
        }
    }
}
