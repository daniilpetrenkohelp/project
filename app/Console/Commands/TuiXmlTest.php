<?php

namespace App\Console\Commands;

use App\Domain\Core\Services\RegistrationService;
use App\Domain\Core\Services\TenantSetupService;
use App\Exceptions\RegistrationEmailAlreadyKnownError;
use Illuminate\Console\Command;
use Symfony\Component\DomCrawler\Crawler;

class TuiXmlTest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ij:tuixml';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Test parsing TUI reservations XML';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(
    )
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $crawlerB = new Crawler($this->fetchTuiXml());
        foreach ($crawlerB as $domElement) {
            print $domElement->nodeName ."\n";
        }

        echo $crawlerB->filter('Reservation > HotelReference > hotelCode')->text() . "\n";
        $crawlerB->filter('Reservation > ResProfiles > ResProfile > Profile[profileType="GUEST"]')->each(function($a, $i) {
            echo ">>> " . $a->filter('Profile > genericName')->text() . "\n";
        });
    }

    private function fetchTuiXml() {
        $xml = <<<'XML'
            <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
            <Reservation
                mfShareAction="NA"
                mfReservationAction="ADD"
                xmlns="Reservation.TUI.Deutschland.GmbH.Version.TUI.1.0"
                xmlns:ns2="profile.TUI.1.0">

                <HotelReference>
                    <hotelCode>TFS31030</hotelCode>
                </HotelReference>
                <confirmationID>76412387A01TFS31030</confirmationID>
                <reservationOriginatorCode>SUPERVISOR</reservationOriginatorCode>
                <mfLegNumbers>1</mfLegNumbers>
                <originalBookingDate>2021-01-11T11:19:00</originalBookingDate>
                <costDate>2021-01-11</costDate>
                <StayDateRange timeUnitType="DAY">
                    <startTime>2021-01-31T00:00:00</startTime>
                    <numberOfTimeUnits>5</numberOfTimeUnits>
                </StayDateRange>
                <GuestCounts>
                    <GuestCount>
                        <ageQualifyingCode>ADULT</ageQualifyingCode>
                        <mfCount>2</mfCount>
                    </GuestCount>
                </GuestCounts>
                <ResGuests>
                    <ResGuest reservationActionType="NA">
                        <resGuestRPH>0</resGuestRPH>
                        <profileRPHs>(0,2)</profileRPHs>
                        <InHouseTimeSpan timeUnitType="DAY">
                            <startTime>2021-01-31T00:00:00</startTime>
                            <numberOfTimeUnits>5</numberOfTimeUnits>
                        </InHouseTimeSpan>
                    </ResGuest>
                    <ResGuest reservationActionType="NA">
                        <resGuestRPH>1</resGuestRPH>
                        <profileRPHs>1</profileRPHs>
                        <InHouseTimeSpan timeUnitType="DAY">
                            <startTime>2021-01-31T00:00:00</startTime>
                            <numberOfTimeUnits>5</numberOfTimeUnits>
                        </InHouseTimeSpan>
                    </ResGuest>
                </ResGuests>
                <ResProfiles>
                    <ResProfile>
                        <ns2:Profile profileType="GUEST" gender="FEMALE">
                            <ns2:profileID>7641238701</ns2:profileID>
                            <ns2:custDBID>249252107</ns2:custDBID>
                            <ns2:creatorCode>SUPERVISOR</ns2:creatorCode>
                            <ns2:genericName>Fischer</ns2:genericName>
                            <ns2:IndividualName>
                                <ns2:nameSalutation>Mrs</ns2:nameSalutation>
                                <ns2:nameFirst>Birgit</ns2:nameFirst>
                                <ns2:nameSur>Fischer</ns2:nameSur>
                            </ns2:IndividualName>
                            <ns2:primaryLanguageID>DE</ns2:primaryLanguageID>
                            <ns2:Comments>
                                <ns2:Comment>
                                    <ns2:commentStr>Age: 60</ns2:commentStr>
                                </ns2:Comment>
                            </ns2:Comments>
                            <ns2:Memberships>
                                <ns2:Membership>
                                    <ns2:programCode>TUI</ns2:programCode>
                                    <ns2:accountID>249252107</ns2:accountID>
                                </ns2:Membership>
                            </ns2:Memberships>
                        </ns2:Profile>
                        <resProfileRPH>0</resProfileRPH>
                    </ResProfile>
                    <ResProfile>
                        <ns2:Profile profileType="GUEST" gender="FEMALE">
                            <ns2:profileID>7641238702</ns2:profileID>
                            <ns2:custDBID>249252107</ns2:custDBID>
                            <ns2:creatorCode>SUPERVISOR</ns2:creatorCode>
                            <ns2:genericName>Test</ns2:genericName>
                            <ns2:IndividualName>
                                <ns2:nameSalutation>Mrs</ns2:nameSalutation>
                                <ns2:nameFirst>Marta</ns2:nameFirst>
                                <ns2:nameSur>Test</ns2:nameSur>
                            </ns2:IndividualName>
                            <ns2:primaryLanguageID>DE</ns2:primaryLanguageID>
                            <ns2:Comments>
                                <ns2:Comment>
                                    <ns2:commentStr>Age: 50</ns2:commentStr>
                                </ns2:Comment>
                            </ns2:Comments>
                        </ns2:Profile>
                        <resProfileRPH>1</resProfileRPH>
                    </ResProfile>
                    <ResProfile>
                        <ns2:Profile profileType="CORPORATE" gender="UNKNOWN">
                            <ns2:profileID>10000000002</ns2:profileID>
                            <ns2:creatorCode>SUPERVISOR</ns2:creatorCode>
                            <ns2:genericName>TUI Deutschland</ns2:genericName>
                            <ns2:IndividualName>
                                <ns2:nameSur>TUI Deutschland</ns2:nameSur>
                            </ns2:IndividualName>
                            <ns2:ElectronicAddresses>
                                <ns2:ElectronicAddress electronicAddressType="EMAIL">
                                    <ns2:eAddress>touristikcom@tui.de</ns2:eAddress>
                                </ns2:ElectronicAddress>
                            </ns2:ElectronicAddresses>
                            <ns2:PostalAddresses>
                                <ns2:PostalAddress>
                                    <ns2:address1>Karl-Wiechert-Allee 23</ns2:address1>
                                    <ns2:city>Hannover</ns2:city>
                                    <ns2:postalCode>30625</ns2:postalCode>
                                    <ns2:countryCode>DE</ns2:countryCode>
                                </ns2:PostalAddress>
                            </ns2:PostalAddresses>
                        </ns2:Profile>
                        <resProfileRPH>2</resProfileRPH>
                    </ResProfile>
                </ResProfiles>
                <RoomStays>
                    <RoomStay reservationActionType="NA" reservationStatusType="RESERVED">
                        <roomStayRPH>0</roomStayRPH>
                        <roomInventoryCode>DZX2</roomInventoryCode>
                        <roomInventoryName>Doppelzimmer Typ2</roomInventoryName>
                        <TimeSpan timeUnitType="DAY">
                            <startTime>2021-01-31T00:00:00</startTime>
                            <numberOfTimeUnits>5</numberOfTimeUnits>
                        </TimeSpan>
                        <GuestCounts>
                            <GuestCount>
                                <ageQualifyingCode>ADULT</ageQualifyingCode>
                                <mfCount>2</mfCount>
                            </GuestCount>
                        </GuestCounts>
                        <RatePlans>
                            <RatePlan reservationActionType="CHANGE">
                                <ratePlanRPH>0</ratePlanRPH>
                                <ratePlanCode>TUI0009H;XL1;XL2;AGH73338</ratePlanCode>
                                <TimeSpan timeUnitType="DAY">
                                    <startTime>2021-01-31T00:00:00</startTime>
                                    <numberOfTimeUnits>1</numberOfTimeUnits>
                                </TimeSpan>
                                <mfMarketCode>TUI</mfMarketCode>
                                <mfSourceCode>13942242</mfSourceCode>
                            </RatePlan>
                            <RatePlan reservationActionType="CHANGE">
                                <ratePlanRPH>0</ratePlanRPH>
                                <ratePlanCode>TUI0009H;XL1;XL2;AGH73338</ratePlanCode>
                                <TimeSpan timeUnitType="DAY">
                                    <startTime>2021-02-01T00:00:00</startTime>
                                    <numberOfTimeUnits>1</numberOfTimeUnits>
                                </TimeSpan>
                                <mfMarketCode>TUI</mfMarketCode>
                                <mfSourceCode>13942242</mfSourceCode>
                            </RatePlan>
                            <RatePlan reservationActionType="CHANGE">
                                <ratePlanRPH>0</ratePlanRPH>
                                <ratePlanCode>TUI0009H;XL1;XL2;AGH73338</ratePlanCode>
                                <TimeSpan timeUnitType="DAY">
                                    <startTime>2021-02-02T00:00:00</startTime>
                                    <numberOfTimeUnits>1</numberOfTimeUnits>
                                </TimeSpan>
                                <mfMarketCode>TUI</mfMarketCode>
                                <mfSourceCode>13942242</mfSourceCode>
                            </RatePlan>
                            <RatePlan reservationActionType="CHANGE">
                                <ratePlanRPH>0</ratePlanRPH>
                                <ratePlanCode>TUI0009H;XL1;XL2;AGH73338</ratePlanCode>
                                <TimeSpan timeUnitType="DAY">
                                    <startTime>2021-02-03T00:00:00</startTime>
                                    <numberOfTimeUnits>1</numberOfTimeUnits>
                                </TimeSpan>
                                <mfMarketCode>TUI</mfMarketCode>
                                <mfSourceCode>13942242</mfSourceCode>
                            </RatePlan>
                            <RatePlan reservationActionType="CHANGE">
                                <ratePlanRPH>0</ratePlanRPH>
                                <ratePlanCode>TUI0009H;XL1;XL2;AGH73338</ratePlanCode>
                                <TimeSpan timeUnitType="DAY">
                                    <startTime>2021-02-04T00:00:00</startTime>
                                    <numberOfTimeUnits>1</numberOfTimeUnits>
                                </TimeSpan>
                                <mfMarketCode>TUI</mfMarketCode>
                                <mfSourceCode>13942242</mfSourceCode>
                            </RatePlan>
                        </RatePlans>
                        <inventoryBlockCode>TUI0009</inventoryBlockCode>
                        <resGuestRPHs>0,1</resGuestRPHs>
                        <GuaranteeInfo guaranteeType="NA">
                            <mfGuaranteeType>B</mfGuaranteeType>
                        </GuaranteeInfo>
                        <mfconfidentialRate>true</mfconfidentialRate>
                        <FacilityList>
                            <facility>Klimaanlage</facility>
                            <facility>Balkon oder Terrasse</facility>
                            <facility>Dusche</facility>
                            <facility>WC</facility>
                            <facility>Wohn-/Schlafraum kombiniert</facility>
                        </FacilityList>
                    </RoomStay>
                </RoomStays>
                <Services>
                    <Service>
                        <serviceInventoryCode>TFS31030</serviceInventoryCode>
                        <serviceInventoryName>Hotel Riu Palace Tenerife</serviceInventoryName>
                    </Service>
                </Services>
                <Agent>
                    <code>021245</code>
                    <contact>0000</contact>
                </Agent>
                <supplierRef></supplierRef>
                <pricingType>CMP</pricingType>
                <promotion Code="0009" Name="P TUID Deutschland"/>
                <InfoList/>
            </Reservation>
XML;
        return $xml;
    }
}
