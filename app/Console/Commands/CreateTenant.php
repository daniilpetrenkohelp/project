<?php

namespace App\Console\Commands;

use App\Domain\Core\Services\RegistrationService;
use App\Domain\Core\Services\TenantSetupService;
use App\Domain\Core\Services\UserService;
use App\Exceptions\RegistrationEmailAlreadyKnownError;
use Illuminate\Console\Command;

class CreateTenant extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ij:tenant:create ${registrantEmail}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create and provision a new tenant application.';
    /**
     * @var RegistrationService
     */
    private $registrationService;
    /**
     * @var TenantSetupService
     */
    private $tenantDataService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(
        RegistrationService $registrationService
    )
    {
        parent::__construct();
        $this->registrationService = $registrationService;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $registrantEmail = $this->argument('registrantEmail');
        echo "creating new tenant for registrant '${registrantEmail}'\n";
        $tenant = null;
        try {
            $tenant = $this->registrationService->setupByMailAndProvision($registrantEmail);
            $tenantId = $tenant->id;
            echo "\n New Tenant ID is: ${tenantId}\n\n";
        } catch(RegistrationEmailAlreadyKnownError $error) {
            echo "Registration with the given E-Mail address already exists. ${registrantEmail}\n";
            exit(1);
        }
        // TODO: create elastic search index for user
        echo "done creating\n";

        return 0;
    }

}
