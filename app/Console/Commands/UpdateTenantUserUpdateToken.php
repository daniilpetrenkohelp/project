<?php

namespace App\Console\Commands;

use App\Domain\Core\Models\User;
use App\Domain\Core\Services\RegistrationService;
use App\Domain\Core\Services\TenantSetupService;
use App\Exceptions\RegistrationEmailAlreadyKnownError;
use Error;
use Illuminate\Console\Command;
use Illuminate\Support\Str;

class UpdateTenantUserUpdateToken extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ij:tenant:user:update-token ${tenantId} ${userEmail}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create user for the given tenant.';
    /**
     * @var RegistrationService
     */
    private $registrationService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(
        RegistrationService $registrationService,
        TenantSetupService $tenantDataService
    )
    {
        parent::__construct();
        $this->registrationService = $registrationService;
        $this->tenantDataService = $tenantDataService;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $tenantId = $this->argument('tenantId');
        $userEmail = $this->argument('userEmail');

        echo "update api token for (${userEmail}) user for tenant '${tenantId}'\n";
        $tenant = null;

        try {
            // TODO: extract into service for shared use with web application
            tenancy()->runForMultiple([$tenantId], function ($tenant) use ($userEmail){
                $user = User::where('email', $userEmail)->firstOrFail();
                $user->api_token = Str::random(60);
                $user->save();
            });
        } catch(Error $err) {
            echo "User with the given E-Mail address does not exist: ${userEmail}\n";
            exit(1);
        }
        // TODO: create elastic search index for user
        echo "done creating\n";

        return 0;
    }
}
