<?php

namespace App\Console\Commands;

use App\Domain\Core\Models\Tenant;
use App\Domain\Core\Models\User;
use App\Domain\Core\Services\RegistrationService;
use App\Domain\Core\Services\TenantService;
use App\Domain\Core\Services\TenantSetupService;
use App\Exceptions\RegistrationEmailAlreadyKnownError;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class ResetTenant extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ij:tenant:reset ${tenantId}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Existing Data will be DELETED! Drops all tables of the given tenant. Migrates to newest version. Populates random data.';

    /**
     * @var TenantSetupService
     */
    private $tenantSetupService;

    /**
     * @var TenantService
     */
    private $tenantService;
    private $registrationService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(
        TenantSetupService $tenantSetupService,
        TenantService $tenantService,
        RegistrationService $registrationService
    )
    {
        parent::__construct();
        $this->tenantService = $tenantService;
        $this->tenantSetupService = $tenantSetupService;
        $this->registrationService = $registrationService;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $tenantId = $this->argument('tenantId');
        echo "recreating the database of tenant '${tenantId}'\n";
        $tenant = null;
        try {
            $tenant = $this->tenantService->findById($tenantId);
            $users = [];
            tenancy()->runForMultiple([$tenant->id], function ($tenant) use (&$users) {
                $users = User::all()->toArray();
            });

            Artisan::call('tenants:migrate-fresh', [
                '--tenants' => [ $tenant->id ],
            ]);

            $this->tenantSetupService->setupTableStructures($tenant);

            $this->recreateBackupedUsers($tenant, $users);
            $this->tenantSetupService->populateTenantSystemTables($tenant);
            $this->tenantSetupService->populateWithRandomData($tenant);
            $this->tenantSetupService->updateModelIndicies($tenant);
        } catch(RegistrationEmailAlreadyKnownError $error) {
            echo "Registration with the given E-Mail address already exists. ${tenant}\n";
            exit(1);
        }
        // TODO: create elastic search index for user
        echo "done creating\n";

        return 0;
    }

    /**
     * @param \App\Domain\Core\Models\Tenant $tenant
     * @param array $users
     * @return array
     */
    private function recreateBackupedUsers(Tenant &$tenant, array &$users): array
    {
        tenancy()->runForMultiple([$tenant->id], function () use (&$users) {
            foreach ($users as $user) {
                if (isset($user['account_id'])) {
                    unset($user['account_id']);
                }

                $user['created_at'] = Carbon::now();
                $user['updated_at'] = null;
                $user['deleted_at'] = null;
                User::insert($user);
            }
        });
        return $users;
    }
}
