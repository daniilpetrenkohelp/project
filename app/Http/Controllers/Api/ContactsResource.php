<?php

namespace App\Http\Controllers\Api;

use App\Domain\Contacts\Models\Entities\Contact;
use App\Domain\Core\Services\ContactService;
use App\Http\Controllers\Controller;
use App\Helpers\Http\IJResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\Response;

class ContactsResource extends Controller
{
    use IJResource;

    private ContactService $contactService;

    public function __construct(
        ContactService $contactService,
    ) {
        $this->contactService = $contactService;
    }

    public function list()
    {
        $contactEntities = Contact::all();
        $contacts = [];

        foreach($contactEntities as &$contactEntity) {

                $combinedName = $contactEntity->company_name;
                if($contactEntity->given_name !== null) {
                    $givenName = $contactEntity->given_name;
                    $familyName = $contactEntity->family_name;
                    $combinedName = "$givenName $familyName";
                }

                $contact = array(
                    'uuid' => $contactEntity->uuid,
                    'type' => $contactEntity->type,
                    'givenName' => $contactEntity->given_name,
                    'familyName' => $contactEntity->family_name,
                    'companyName' => $contactEntity->company_name,
                    'combinedName' => $combinedName,
                );
                array_push($contacts, $contact);
        }

        $asFormated = request()->wantsJson() ? 0 : JSON_PRETTY_PRINT;
        return response()->json($contacts, 200, [], $asFormated);
    }

    public function create(Request $request)
    {
        Log::debug('enter ContactsResource::create');

        $contactDto = $request->all();
        $contact = $this->contactService->createContactByConfig($contactDto);
        $contactResponseDto = json_decode(json_encode($contact), associative: true);

        return $this->respondWithJSON($contactResponseDto, Response::HTTP_OK);
    }

    public function read_one()
    {
        $asFormated = request()->wantsJson() ? 0 : JSON_PRETTY_PRINT;
        return response()->json(null, 200, [], $asFormated);
    }

    public function update(Request $request, string $uuid)
    {
        Log::debug('enter ContactsResource::update');

        // TODO: extract update into service
        $contact = Contact::where('uuid', $uuid)->firstOrFail();
        $contactUpdateDto = $request->all();
        $contact->fill($contactUpdateDto);
        $contact->save();

        $contactResponseDto = json_decode(json_encode($contact), true);
        unset($contactResponseDto['deleted_at']);
        $asFormated = request()->wantsJson() ? 0 : JSON_PRETTY_PRINT;
        return response()->json($contactResponseDto, 200, [], $asFormated);
    }


}
