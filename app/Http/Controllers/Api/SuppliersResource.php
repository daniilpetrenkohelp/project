<?php

namespace App\Http\Controllers\Api;

use App\Domain\Suppliers\Services\SupplierService;
use App\Exceptions\DomainObjectCreationException;
use App\Exceptions\DomainObjectDuplicateException;
use App\Exceptions\DomainObjectInvalidException;
use App\Exceptions\DomainObjectNotDeletedException;
use App\Exceptions\DomainObjectNotFoundException;
use App\Http\Controllers\Controller;
use App\Helpers\Http\APIEffectStates;
use App\Helpers\Http\IJResource;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Request;

class SuppliersResource extends Controller
{

    use IJResource;

    /**
     * @var SupplierService
     */
    private $supplierService;

    public function __construct(
        SupplierService $supplierService
    ) {
        $this->supplierService = $supplierService;
    }

    public function create(Request $request)
    {
        Log::debug('enter SuppliersResource::create');
        $asFormated = request()->wantsJson() ? 0 : JSON_PRETTY_PRINT;

        $supplierRequestDto = $request->all();

        // TODO: extract generating kode into model creation
        $supplierRequestDto = array_merge($supplierRequestDto, array(
            'kode' => Str::random('6'),
        ));

        try {
            $supplierEntity = $this->supplierService->createByConfig($supplierRequestDto);
            $jsonResponse = json_decode(json_encode($supplierEntity), associative: true);
            $associatecAddress = $supplierEntity->address()->get()->first();
            $jsonResponse['address'] = json_decode(json_encode($associatecAddress), associative: true);
            $httpResponseCode = Response::HTTP_OK;
        } catch (DomainObjectCreationException |
                 DomainObjectDuplicateException |
                 DomainObjectInvalidException $error) {
            $httpResponseCode = Response::HTTP_INTERNAL_SERVER_ERROR;
            $jsonResponse = array(
                'uuid'   => null,
                'state'  => APIEffectStates::OUTCOME_STATE_UNCHANGED,
                'reason' => $error->getMessage(),
            );
        }

        return $this->respondWithJSON($jsonResponse, $httpResponseCode);
    }

    public function list()
    {
        Log::debug('enter SuppliersResource::list');

        $allSupplierEntities = $this->supplierService->readAllSuppliers();
        $allSuppliersJSON = array();
        foreach($allSupplierEntities as &$supplierEntity) {
            $allSuppliersJSON[] = json_decode(json_encode($supplierEntity), true);
        }

        $listSuppliersResponse = array(
            'size' => count($allSuppliersJSON),
            'data' => $allSuppliersJSON,
        );

        return $this->respondWithJSON($listSuppliersResponse,Response::HTTP_OK);
    }

    public function read(string $uuid)
    {
        Log::debug("enter SuppliersResource::readOne uuid:${uuid}");

        $httpResponseCode = null;
        $readSupplierResponse = null;

        try {
            $supplierEntity = $this->supplierService->readSupplierByUUID($uuid);
            $readSupplierResponseDTO = json_decode(json_encode($supplierEntity));

            $httpResponseCode = Response::HTTP_OK;
            $readSupplierResponse = $readSupplierResponseDTO;
        } catch (DomainObjectNotFoundException $error) {
            $httpResponseCode = Response::HTTP_NOT_FOUND;
            $readSupplierResponse = array(
                'uuid'   => $uuid,
                'state'  => APIEffectStates::OUTCOME_STATE_NOT_FOUND,
                'reason' => $error->getMessage(),
            );
        }

        return $this->respondWithJSON($readSupplierResponse, $httpResponseCode);
    }

    public function update(Request $request, string $uuid)
    {
        Log::debug('enter SuppliersResource::update');

        // TODO extract to update supplier
        $supplierEntity = $this->supplierService->readSupplierByUUID($uuid);
        $supplierUpdatesDTO = $request->all();
        $supplierEntity->fill($supplierUpdatesDTO);
        $supplierEntity->save();

        $contactResponseDto = json_decode(json_encode($supplierEntity));
        return $this->respondWithJSON($contactResponseDto, Response::HTTP_OK);
    }

    public function delete(string $uuid)
    {
        Log::debug('enter SuppliersResource::delete');

        $httpResponseCode = null;
        $deleteSupplierResponse = null;

        try {
            $deletedSupplierEntity = $this->supplierService->deleteSupplierByUUID($uuid);
            $httpResponseCode = Response::HTTP_OK;
            $deleteSupplierResponse = array(
                'uuid'   => $deletedSupplierEntity->uuid,
                'state'  =>  APIEffectStates::OUTCOME_STATE_DELETED,
            );
        } catch (DomainObjectNotDeletedException $error) {
            $httpResponseCode = Response::HTTP_INTERNAL_SERVER_ERROR;
            $deleteSupplierResponse = array(
                'uuid'   => $uuid,
                'state'  => APIEffectStates::OUTCOME_STATE_UNCHANGED,
                'reason' => $error->getMessage(),
            );
        } catch (DomainObjectNotFoundException $error) {
            $httpResponseCode = Response::HTTP_NOT_FOUND;
            $deleteSupplierResponse = array(
                'uuid'   => $uuid,
                'state'  => APIEffectStates::OUTCOME_STATE_NOT_FOUND,
                'reason' => $error->getMessage(),
            );
        }

        return $this->respondWithJSON($deleteSupplierResponse, $httpResponseCode);
    }

}
