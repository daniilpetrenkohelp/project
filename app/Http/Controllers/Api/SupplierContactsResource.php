<?php

namespace App\Http\Controllers\Api;

use App\Domain\Contacts\Models\Entities\SupplierContact;
use App\Domain\Core\Models\Supplier;
use App\Domain\Core\Services\ContactService;
use App\Domain\Core\Services\SupplierContactService;
use App\Exceptions\DomainObjectNotFoundException;
use App\Http\Controllers\Controller;
use App\Helpers\Http\APIEffectStates;
use App\Helpers\Http\IJResource;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\Response;

class SupplierContactsResource extends Controller
{
    use IJResource;

    private SupplierContactService $supplierContactService;
    private ContactService $contactService;

    public function __construct(
        SupplierContactService $supplierContactService,
        ContactService $contactService,
    )
    {
        $this->supplierContactService = $supplierContactService;
        $this->contactService = $contactService;
    }

    public function create(Request $request, string $supplierUUID)
    {
        Log::debug('enter SupplierContactsResource::create');

        try {
            $supplierContactRequestDto = $request->all();
            $supplierContactRequestDto = array_merge($supplierContactRequestDto,[
                'supplier_uuid' => $supplierUUID,
            ]);

            $supplierContactEntity = $this->supplierContactService->createContactAtSupplier($supplierContactRequestDto);
            $jsonResponse = $this->renderSupplierContactJSON($supplierContactEntity);
            $httpResponseCode = Response::HTTP_OK;
        } catch (DomainObjectNotFoundException $error) {
            $httpResponseCode = Response::HTTP_NOT_FOUND;
            $jsonResponse = array(
                'uuid' => $supplierUUID,
                'state' => APIEffectStates::OUTCOME_STATE_NOT_FOUND,
                'reason' => $error->getMessage(),
            );
        }

        return $this->respondWithJSON($jsonResponse, $httpResponseCode);
    }

    public function list(string $supplierUUID)
    {
        Log::debug('enter SuppliersContactsResource::list');

        $supplierEntity = null;
        $httpResponseCode = null;
        $jsonResponse = null;

        try {
            $supplierEntity = Supplier::where('uuid', $supplierUUID)->firstOrFail();
            $supplierRoleEntities = $supplierEntity->roles()->get();

            throw new \Exception('Must be implemented!');
//        $allSupplierEntities = $this->supplierService->readAllSuppliers();
//        $allSuppliersJSON = array();
//        foreach($allSupplierEntities as &$supplierEntity) {
//            $allSuppliersJSON[] = json_decode(json_encode($supplierEntity), true);
//        }
            $allSupplierContactsJSON = [];
            $jsonResponse = array(
                'size' => count($allSupplierContactsJSON),
                'data' => $allSupplierContactsJSON,
            );

        } catch (ModelNotFoundException $error) {
            Log::debug(print_r($error, true));
            $httpResponseCode = Response::HTTP_NOT_FOUND;
            $jsonResponse = array(
                'uuid' => null,
                'state' => APIEffectStates::OUTCOME_STATE_NOT_FOUND,
                'reason' => $error->getMessage(),
            );
        }

        return $this->respondWithJSON($jsonResponse, $httpResponseCode);
    }

    public function read(string $uuid)
    {
        $supplierContactEntity = $this->contactService->readSupplierRole($uuid);

        $supplierContactJson = $this->renderSupplierContactJSON($supplierContactEntity);
        $jsonView = $supplierContactJson;

        $asFormated = request()->wantsJson() ? 0 : JSON_PRETTY_PRINT;
        return response()->json($jsonView, 200, [], $asFormated);
    }

    /**
     * @param $supplierContactEntity
     * @return mixed
     */
    private function renderSupplierContactJSON(SupplierContact &$supplierContactEntity) : mixed
    {
        // TODO: view timestamps == total latest timestamps (iterate and collect from every subentity)
        // TODO: implement chacing ID for resources? checksum from several properties like timestamps and UUIDs?
        $contactEntity = $supplierContactEntity->contact()->get()->first();
        $contactLocations = $contactEntity->locations()->get();
        $contactChannels = $contactEntity->contactDetails()->get();
        $financialDetails = $contactEntity->financialDetails()->get();
        $supplierEntity = $supplierContactEntity->supplier()->first();

        $supplierContactJson = $supplierContactEntity->toArray();
        // TODO: distinguish if related role contact is a company or a natural person.
        $supplierContactJson['supplier'] = $supplierEntity->toArray();
        $supplierContactJson['contact'] = $contactEntity->toArray();
        $supplierContactJson['locations'] = $contactLocations;
        $supplierContactJson['financial_details'] = $financialDetails;
        $supplierContactJson['channels'] = $contactChannels->toArray();
        $supplierContactJson['roles'] = $supplierContactEntity->roles;

        unset($supplierContactJson['supplier_id']);
        unset($supplierContactJson['contact_id']);

        return $supplierContactJson;
    }

}
