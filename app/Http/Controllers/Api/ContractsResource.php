<?php

namespace App\Http\Controllers\Api;

use App\Domain\Core\Services\ContactService;
use App\Domain\Core\Services\PDFGeneratorService;
use App\Domain\Sourcing\Models\Contract;
use App\Domain\Sourcing\Services\ContractService;
use App\Helpers\Http\IJResource;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\Response;


class ContractsResource extends Controller
{
    use IJResource;

    private $contractService;
    /**
     * @var ContractService
     */
    private $contactService;
    /**
     * @var ContactService
     */
    private $PDFGeneratorService;
    /**
     * @var PDFGeneratorService
     */

    public function __construct(
        ContactService $contactService,
        ContractService $contractService,
        PDFGeneratorService $PDFGeneratorService
    ) {
        $this->contactService = $contactService;
        $this->contractService = $contractService;
        $this->PDFGeneratorService = $PDFGeneratorService;
    }

    public function list() {
        $contractEntities = Contract::all();
        $contracts = [];

        foreach($contractEntities as &$contractEntity) {
            $supplier = $contractEntity->supplier()->first();
            $supplierName = '-';
            if($supplier !== null) {
                $supplierName = $supplier->name;
            }

            $contract = array(
                'uuid' => $contractEntity->uuid,
                'kode' => $contractEntity->kode,
                'supplierName' => $supplierName,
                'createdAt' => $contractEntity->created_at,
            );
            array_push($contracts, $contract);
        }

        $jsonView = array(
            'data' => $contracts,
        );

        $asFormated = request()->wantsJson() ? 0 : JSON_PRETTY_PRINT;
        return response()->json($jsonView, 200, [], $asFormated);
    }

    public function create(Request $request)
    {
        Log::debug('enter ContractsResource->create');
        $data = $request->json()->all();
        Log::debug(print_r($data,true));

        $supplierContactUUID = $data['contract_partner']['supplier_contact_uuid'];
        $supplierContact = $this->contactService->readSupplierOfSupplierContactUUID($supplierContactUUID);

        $randomKode = explode('-', Str::uuid());
        $randomKode = array_pop($randomKode);

        $contract = new Contract();
        $contract->uuid = Str::uuid();
        $contract->document = $data;
        $contract->kode = $randomKode;
        $contract->save();

        $jsonResponse = $contract->asContractDocument();
        $httpResponseCode = Response::HTTP_OK;
        return $this->respondWithJSON($jsonResponse, $httpResponseCode);
    }

    public function read(Request $request, string $uuid)
    {
        $contract = $this->contractService->readContractByUUID($uuid);
        return response()->json($contract->asContractDocument(), 200, [], JSON_PRETTY_PRINT);
    }
    public function exportPDF($uuid){
        $contract = Contract::where('uuid', $uuid)->firstOrFail();
        $filename = sha1($contract->uuid.$contract->updated_at).".pdf";//generating a name of a PDF
        return $this->PDFGeneratorService->generateSellingContract($uuid, $filename);



    }

}
