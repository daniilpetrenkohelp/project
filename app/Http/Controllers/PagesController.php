<?php

namespace App\Http\Controllers;

use Inertia\Inertia;

class PagesController extends Controller
{
    public $layout = 'layouts.public';

    public function terms()
    {
        return 'Terms and Conditions Page';
    }

    public function gdpr()
    {
        return 'GDPR Page';
    }

    public function imprint()
    {
        return 'Imprint Page';
    }

}
