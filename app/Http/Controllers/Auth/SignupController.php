<?php

namespace App\Http\Controllers\Auth;

use App\Domain\Core\Services\RegistrationService;
use App\Domain\Core\Services\TenantSetupService;
use App\Exceptions\RegistrationAlreadyConfirmedError;
use App\Exceptions\RegistrationCodeUnkownError;
use App\Exceptions\RegistrationEmailAlreadyKnownError;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;

class SignupController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Signup Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    protected $layout = 'public';

    /**
     * @var RegistrationService
     */
    private $registrationService;
    /**
     * @var TenantSetupService
     */
    private $tenantSetupService;

    public function __construct(
        RegistrationService $registrationService,
        TenantSetupService $tenantSetupService
    ) {
//        die();
        $this->registrationService = $registrationService;
        $this->tenantSetupService = $tenantSetupService;
    }

    /**
     * Show the application's login form.
     *
     * @return \Inertia\Response
     */
    public function showSignupForm() {
        return Inertia::render('Auth/Signup');
    }

    public function createRegistrationRequest(Request $request) {
        $validated = $request->validate([
            'email' => ['required', 'min:5', 'email'],
        ]);

        $registrantEmail = $validated['email'];

        try {
            $this->registrationService->prepareTenantRegistration($registrantEmail);
        } catch(RegistrationEmailAlreadyKnownError $error) {
            Log::error('RegistrantAlreadySignedUpError with ' . $registrantEmail );
            return Redirect::back()
                ->withErrors([
                    'email' => 'this domain is already registered',
                ]);
        }

        return redirect()->route('registration.pending');
    }

    public function waitingForConfirmation() {
        return Inertia::render('Signup/Success');
    }

    public function confirmRegistrationRequest(string $registrationCode) {
        try {
            $tenant = $this->registrationService->confirmTenantRegistration($registrationCode);
            $this->registrationService->setupTenantEnvironment($tenant);

            $tenantDomain = $tenant->domains()->first();
            $this->tenantSetupService->createInitialUser($tenant);

            $domain = $tenantDomain->domain;
            $domain = "https://${domain}";
            return Redirect::to($domain);
        } catch(RegistrationCodeUnkownError $error) {
            return "registration code '${registrationCode}' is unknown.";
        } catch(RegistrationAlreadyConfirmedError $error) {
            return "already confirmed. please login.";
        }
    }

}
