<?php

namespace App\Http\Controllers\Workflow;

use App\Domain\Contacts\Models\Entities\Contact;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use Inertia\Inertia;

class TestController extends Controller
{
    public function search()
    {
        $searchQuery = '';
        $allowedParams = Request::only(['search']);
        if(array_key_exists('search', $allowedParams)) {
            $searchQuery = $allowedParams['search'];
        }

        $aticles = Contact::search($searchQuery);


      Auth::user()->account->contacts()
            ->with('organization')
            ->orderByName()
            ->filter(Request::only('search', 'trashed'));

      $result =  $aticles->paginate()
            ->withQueryString()
            ->through(function ($contact) {
                return [
                    'id' => $contact->id,
                    'name' => $contact->name,
                    'phone' => $contact->phone,
                    'city' => $contact->city,
                    'deleted_at' => $contact->deleted_at,
                    'organization' => $contact->organization ? $contact->organization->only('name') : null,
                ];
            });

        return Inertia::render('Test/Search', [
            'filters' => Request::all('search', 'trashed'),
            'contacts' => $result,
        ]);
    }
}
