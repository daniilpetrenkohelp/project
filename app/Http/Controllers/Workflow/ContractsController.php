<?php

namespace App\Http\Controllers\Workflow;

use App\Domain\Core\Services\ContactService;
use App\Domain\Core\Models\Country;
use App\Domain\Sourcing\Models\Contract;
use App\Domain\Sourcing\Services\ContractService;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Str;
use Inertia\Inertia;

class ContractsController extends Controller
{

    /**
     * @var ContractService
     */
    private $contractService;

    public function __construct(
        ContractService $contractService,
        ContactService $contactService
    )
    {
        $this->contractService = $contractService;
        $this->contactsService = $contactService;
    }

    // http request handlers

    public function index()
    {
        return Inertia::render('Contracts/Index');
    }

    public function create()
    {
        $contractDTO = array(
            'name' => null,
            'kode' => null,
        );

        $contractDocument = [
            "kode" => null,
            "type" => "sourcing",
            "_meta" => [
                "version" => "0.0.0",
                "requires" => [
                    [ "type" => "plugin", "identifier" => "italy-pec" ],
                ],
            ],
            "supplier" => [
                "address" => [
                    "city" => "Anywhere",
                    "name" => "Meier",
                    "address" => "Hauptstrasse 4",
                    "zipcode" => "45138",
                    "contry_iso_code" => "DE"
                ],
                "supplier_uuid" => "xxxx-xxxxx-xxxxx-xxxx",
                "supplier_contact_uuid" => "xxxx-xxxxx-xxxxx-xxxx",
            ]
        ];

        $params = $this->mergeWithSharedParams([
            'contract' => array_merge($contractDocument, $contractDTO),
        ]);

        return Inertia::render('Contracts/Create', $params);
    }

    public function store(Request $request)
    {
        Log::debug('enter ContractsController::store');

        $data = json_decode($request->payload, true);
        $contract = new Contract();
        $contract->document = $data;
        $contract->uuid = Str::uuid();
    }

    public function edit(string $uuid)
    {
        $contractEntity = $this->contractService->readContractByUUID($uuid);
        $contractDTO = [
            'kode' => $contractEntity->kode,
        ];

        $contractDocument = $contractEntity->document;
        $params = $this->mergeWithSharedParams([
            'contract' => array_merge($contractDocument, $contractDTO),
        ]);

        return Inertia::render('Contracts/Edit', $params);
    }

    public function update(Contract $contract)
    {
        $validatedRequest = Request::validate([
            'name' => ['required', 'max:100'],
        ]);

        $this->contractService->updateContractWithForm($contract, $validatedRequest);
        return Redirect::back()->with('success', 'Gespeichert');
    }

    // supporting methods

    protected function mergeWithSharedParams($params)
    {
        $supplierPrimaryContacts = $this->contactsService->readContractPartnerContacts();
        $countries = Country::all()
            ->map
            ->only('id', 'shortname_en', 'iso_code');

        $formData = array(
            'supplierAddresses' => $supplierPrimaryContacts,
            'countries' => $countries,
        );

        return array_merge($formData, $params);
    }

}
