<?php

namespace App\Http\Controllers\Workflow;

use App\Http\Controllers\Controller;
use Inertia\Inertia;

class SuppliersController extends Controller
{
    public function index()
    {
        return Inertia::render('Suppliers/Index');
    }
}
