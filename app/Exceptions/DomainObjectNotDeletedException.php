<?php


namespace App\Exceptions;


class DomainObjectNotDeletedException extends \Exception
{

    /**
     * BusinessObjectNotDeletedException constructor.
     * @param string $message
     * @param int $code
     * @param \Exception|null $previous
     */
    public function __construct(string $message, $code = 0, \Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

}
