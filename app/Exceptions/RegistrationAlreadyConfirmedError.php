<?php


namespace App\Exceptions;


class RegistrationAlreadyConfirmedError extends \Exception
{

    /**
     * RegistrationAlreadyConfirmedError constructor.
     */
    public function __construct()
    {
    }
}
