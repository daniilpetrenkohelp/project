<?php


namespace App\Exceptions;


class RegistrationEmailAlreadyKnownError extends \Exception
{

    /**
     * AlreadyRegisteredError constructor.
     */
    public function __construct(string $message, $code = 0, \Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

}
