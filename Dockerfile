FROM registry.gitlab.com/incomingjet/docker-php-base

COPY --chown=docker:docker . .

RUN composer install
RUN npm install &&\
    npm run dev

