<?php

namespace Tests\Api;

use App\Domain\Contacts\Models\Entities\Contact;
use App\Domain\Contacts\Models\Entities\ContactDetail;
use App\Domain\Contacts\Models\Entities\SupplierContact;
use App\Helpers\Http\APIEffectStates;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Testing\Fluent\AssertableJson;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class SupplierContactsResourceTest extends TestCase
{

    use DatabaseTransactions;

    /**
     * @var string
     */
    private $resourceBaseName;

    function setUp(): void
    {
        parent::setUp();
        $this->resourceBaseName = 'api.supplier-contacts';
    }

    function tearDown(): void
    {
        parent::tearDown();
    }

    /**
     * @return void
     */
    public function todo_test_listing_of_suppliers_is_available()
    {
        $params = [
            'supplierUUID' => '123-123-123',
        ];
        $endpointUrl = $this->generateTenantApiRoute($this->resourceBaseName . ".list", $params);
        $apiToken = $this->currentUser->api_token;
        $suppliersListingResponse = $this->actingAs($this->currentUser)->json(method: 'get', uri: $endpointUrl, headers: [
            "Authorization" => "Bearer ${apiToken}",
        ]);

        $suppliersListingJSON = $suppliersListingResponse->json();
        if ($this->showDebugOutput === true) {
            echo "\n\nListing of all Supplier Contacts response is:\n";
            echo substr(print_r($suppliersListingJSON, true), 0, 250);
            echo "\n\n";
        }

        $suppliersListingResponse
            ->assertStatus(Response::HTTP_OK)
            ->assertJson(fn(AssertableJson $json) => $this->assertApiResourceListingFormat($json)
            );
    }

    /**
     * @return void
     */
    public function test_expect_error_404_on_non_existing_supplier_in_supplier_contacts_listing()
    {
        $params = [
            'supplierUUID' => 'does-not-exist'
        ];
        $suppliersListResponse = $this->ijTenantApiRequest(
            method: 'GET',
            route: $this->resourceBaseName . ".list",
            routeParams: $params,
        );

        $this->responseDebugLog(
            message: 'Listing of all Supplier Contacts response is:',
            response: $suppliersListResponse);

        $suppliersListResponse
            ->assertJson(fn(AssertableJson $json) => $this->assertApiMessageFormat(
                assertableJson: $json,
                expectedOutcomeState: APIEffectStates::OUTCOME_STATE_NOT_FOUND,
            )
        );
    }

    /**
     * @return void
     */
    public function test_create_supplier_contact_with_ansprechpartner_roles()
    {
        $expectedName = 'Petra Peterson';
        $expectedEmailValue = 'hello@ijstage.com';
        $expectedPhoneValue = '+49 231 27 61 42';

        $minimalSupplierDto = array(
            'name' => 'Minimal Hotel',
        );
        $createSupplierResponse = $this->ijTenantApiRequest(
            method: 'POST',
            route: 'api.suppliers.create',
            data: $minimalSupplierDto,
        );

        $this->responseDebugLog(
            message: 'Created temporary supplier:',
            response: $createSupplierResponse);

        $createSupplierResponse->assertStatus(Response::HTTP_OK);
        $supplierCreationResponse = $createSupplierResponse->json();

        $routeParams = [
            'uuid' => $supplierCreationResponse['uuid'],
        ];
        $supplierContactDTO = [
            'name' => $expectedName,
            'type' => Contact::TYPE_PERSON,
            'roles' => [
                SupplierContact::ROLE_PRIMARY_CONTACT,
                SupplierContact::ROLE_OPERATING_COMPANY,
                SupplierContact::ROLE_FRONTDESK,
            ],
            'channels' => [
                [
                    'type' => ContactDetail::TYPE_EMAIL,
                    'value' => $expectedEmailValue,
                ],
                [
                    'type' => ContactDetail::TYPE_PHONE,
                    'value' => $expectedPhoneValue,
                ],
            ],
        ];

        $contactCreationResponse = $this->ijTenantApiRequest(
            method: 'POST',
            route: $this->resourceBaseName . '.create',
            routeParams: $routeParams,
            data: $supplierContactDTO,
        );

        $this->responseDebugLog(
            message: 'Result of Supplier Contacts creation:',
            response: $contactCreationResponse);

        $contactCreationResponse
            ->assertStatus(Response::HTTP_OK)
            ->assertJson(fn (AssertableJson $json) =>
                $json
                    ->where('supplier.uuid', $supplierCreationResponse['uuid'])
                    ->has('contact.uuid')
                    ->where('contact.name', $expectedName)
                    ->has('channels')
                    ->where('channels.0.type', ContactDetail::TYPE_EMAIL)
                    ->where('channels.0.value', $expectedEmailValue)
                    ->where('channels.1.type', ContactDetail::TYPE_PHONE)
                    ->where('channels.1.value', $expectedPhoneValue)
                    ->has('roles')
                    ->whereContains('roles', array())
                    ->etc()
            );
        $this->assertTrue(in_array(SupplierContact::ROLE_PRIMARY_CONTACT, $contactCreationResponse['roles']));
        $this->assertTrue(in_array(SupplierContact::ROLE_OPERATING_COMPANY, $contactCreationResponse['roles']));

    }

    public function todo_supplier_contact_must_always_have_at_least_one_role_during_creation() {
        $minimalSupplierDto = array(
            'name' => 'Minimal Hotel 2',
        );
        $createSupplierResponse = $this->ijTenantApiRequest(
            method: 'POST',
            route: 'api.suppliers.create',
            data: $minimalSupplierDto,
        );

        $this->responseDebugLog(
            message: 'Created temporary supplier:',
            response: $createSupplierResponse);

        $createSupplierResponse->assertStatus(200);
        $creationResponseJSON = $createSupplierResponse->json();

        $routeParams = [
            'uuid' => $creationResponseJSON['uuid'],
        ];
        $supplierContactDTO = [
            'name' => 'Petra Peter',
            'type' => Contact::TYPE_PERSON,
            'roles' => [
            ],
            'channels' => [
                [
                    'type' => ContactDetail::TYPE_EMAIL,
                    'value' => 'hello@ijstage.com',
                ],
            ],
        ];

        $contactCreationResponse = $this->ijTenantApiRequest(
            method: 'POST',
            route: $this->resourceBaseName . '.create',
            routeParams: $routeParams,
            data: $supplierContactDTO,
        );

        $this->responseDebugLog(
            message: 'Result of Supplier Contacts creation:',
            response: $contactCreationResponse);

        $contactCreationResponse
            ->assertStatus(200);
    }

    public function todo_supplier_contact_must_always_have_at_least_one_role_during_update() {

    }

    public function todo_supplier_contact_details_fields_must_be_updateable() {

    }

    public function todo_supplier_contact_details_fields_must_be_deleteable() {

    }

}
