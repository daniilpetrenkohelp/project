<?php

namespace Tests\Api;

use App\Domain\Contacts\Models\Entities\Contact;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class ContactsResourceTest extends TestCase
{

    use DatabaseTransactions;

    function setUp(): void
    {
        parent::setUp();
        DB::beginTransaction();
    }

    function tearDown(): void
    {
        DB::rollback();
        parent::tearDown();
    }

    /**
     * @return void
     */
    public function xtest_listing_to_be_empty()
    {
        $endpointUrl = $this->generateTenantApiRoute('api.contacts');
        $apiToken = $this->currentUser->api_token;

        $response = $this->actingAs($this->currentUser)->json(method: 'get', uri: $endpointUrl, headers: [
            "Authorization" => "Bearer ${apiToken}",
        ]);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([]);

        $jsonResponse = $response->json();
        $this->assertEquals(count($jsonResponse), 0);
    }

    /**
     * @return void
     */
    public function xtest_create_a_person_contact()
    {
        $endpointUrl = $this->generateTenantApiRoute('api.contacts');
        $apiToken = $this->currentUser->api_token;

        $contactDto = array(
            'type' => Contact::TYPE_PERSON,
            'family_name' => 'Murray',
            'given_name' => 'Bill',
        );

        $response = $this->actingAs($this->currentUser)->json(
            method: 'post',
            uri: $endpointUrl,
            data: $contactDto,
            headers: [
                "Authorization" => "Bearer ${apiToken}",
            ]);

        $response->assertStatus(200)
            ->assertJsonStructure([])
            ->assertJson(fn (AssertableJson $json) =>
                $json->has('uuid')
                    ->where('type', Contact::TYPE_PERSON)
                    ->where('family_name', 'Murray')
                    ->where('given_name', 'Bill')
                    ->has('created_at')
                    ->has('updated_at')
                    ->missing('deleted_at')
                    ->etc()
            );

//        echo "\n\n";
//        print_r($response->content());
    }

    /**
     * @return void
     */
    public function xtest_update_a_person_contact()
    {
        $createContactUrl = $this->generateTenantApiRoute('api.contacts');
        $apiToken = $this->currentUser->api_token;

        $contactDto = array(
            'type' => Contact::TYPE_PERSON,
            'family_name' => 'Peter',
            'given_name' => 'Jordan',
        );

        $response = $this->actingAs($this->currentUser)->json(
            method: 'post',
            uri: $createContactUrl,
            data: $contactDto,
            headers: [
                "Authorization" => "Bearer ${apiToken}",
        ]);

        $response->assertStatus(200)
            ->assertJsonStructure([])
            ->assertJson(fn (AssertableJson $json) =>
            $json->has('uuid')
                ->where('type', Contact::TYPE_PERSON)
                ->where('family_name', 'Peter')
                ->where('given_name', 'Jordan')
                ->has('created_at')
                ->has('updated_at')
                ->missing('deleted_at')
                ->etc()
            );

        $contactResponseDto = json_decode($response->content(), true);

//        echo "\n\n";
//        print_r($response->content());

        $updatedContactDto = array_merge($contactResponseDto, array(
            'family_name' => 'Jordan',
            'given_name' => 'Peterson',
        ));
        $updateContactUrl = $this->generateTenantApiRoute('api.contacts.update', array(
            'uuid' =>$contactResponseDto['uuid'],
        ));
        $response = $this->actingAs($this->currentUser)->json(
            method: 'put',
            uri: $updateContactUrl,
            data: $updatedContactDto,
            headers: [
                "Authorization" => "Bearer ${apiToken}",
        ]);

//        echo "\n\n";
//        print_r($response->content());

        $response->assertStatus(200)
            ->assertJsonStructure([])
            ->assertJson(fn (AssertableJson $json) =>
            $json->has('uuid')
                ->where('type', Contact::TYPE_PERSON)
                ->where('family_name', 'Jordan')
                ->where('given_name', 'Peterson')
                ->where('created_at', $contactResponseDto['created_at'])
                ->has('updated_at')
                ->missing('deleted_at')
                ->etc()
            );
    }
}
