<?php

namespace Tests\Api;

use App\Domain\Contacts\Models\Entities\Location;
use App\Helpers\Http\APIEffectStates;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class SuppliersResourceTest extends TestCase
{

    use DatabaseTransactions;

    /**
     * @var string
     */
    private $resourceBaseName;

    function setUp(): void
    {
        parent::setUp();
        $this->resourceBaseName = 'api.suppliers';
    }

    function tearDown(): void
    {
        parent::tearDown();
    }

    /**
     * @return void
     */
    public function test_listing_of_suppliers_is_available()
    {
        $suppliersListingResponse = $this->ijTenantApiRequest(
            method: 'GET',
            route: $this->resourceBaseName . ".list",
            routeParams: [],
            headers:[],
        );

        $suppliersListingJSON = $suppliersListingResponse->json();
        if($this->showDebugOutput === true) {
            echo "\n\nListing of all Suppliers response is:\n";
            print_r($suppliersListingJSON);
        }

        $suppliersListingResponse
            ->assertStatus(200)
            ->assertJson(fn (AssertableJson $assertableJson) =>
                $this->assertApiResourceListingFormat(
                    assertableJson: $assertableJson,
                    listResponseJSON: $suppliersListingJSON,
                )
            );
    }

    /**
     * @return void
     */
    public function test_create_a_minimal_supplier()
    {
        $expectedSupplierName = 'Schöner Baum Hotel';
        $minimalSupplierDto = array(
            'name' => $expectedSupplierName,
        );

        $response = $this->ijTenantApiRequest(
            method: 'POST',
            route: $this->resourceBaseName . ".create",
            data: $minimalSupplierDto,
        );

        if($this->showDebugOutput === true) {
            echo "\n\n";
            print_r($response->content());
        }

        $response->assertStatus(200)
            ->assertJsonStructure([])
            ->assertJson(fn (AssertableJson $json) =>
                $json->has('uuid')
                    ->has('kode')
                    ->where('name', $expectedSupplierName)
                    ->has('created_at')
                    ->has('updated_at')
                    ->missing('deleted_at')
                    ->etc()
            );

        if($this->showDebugOutput === true) {
            echo "\n\n";
            print_r($response->content());
        }
    }

    /**
     * @return void
     */
    public function test_on_empty_supplier_fail_with_formated_error()
    {
        $emptySupplierDTO = array(
        );

        $response = $this->ijTenantApiRequest(
            method: 'POST',
            route: $this->resourceBaseName . ".create",
            data: $emptySupplierDTO,
        );

        if($this->showDebugOutput === true) {
            echo "\n\n";
            print_r($response->content());
        }

        $response->assertStatus(500)
            ->assertJsonStructure([])
            ->assertJson(fn (AssertableJson $assertableJson) =>
                $this->assertApiMessageFormat(
                    assertableJson: $assertableJson,
                    expectedOutcomeState:  APIEffectStates::OUTCOME_STATE_UNCHANGED,
                )
            );

        if($this->showDebugOutput === true) {
            echo "\n\n";
            print_r($response->content());
        }
    }


    public function test_create_and_read_a_minimal_supplier()
    {
        $expectedSupplierName = 'Blauer Baum Hotel';
        $minimalSupplierDto = array(
            'name' => $expectedSupplierName,
        );

        $createSupplierResponse = $this->ijTenantApiRequest(
            method: 'POST',
            route: $this->resourceBaseName . ".create",
            data: $minimalSupplierDto,
        );

        if($this->showDebugOutput === true) {
            echo "\n\n";
            print_r($createSupplierResponse->json());
        }

        $createSupplierResponse->assertStatus(200)
            ->assertJsonStructure([])
            ->assertJson(fn (AssertableJson $json) =>
            $json->has('uuid')
                ->etc()
            );

        $params = array(
            'uuid' => $createSupplierResponse->json()['uuid'],
        );
        $readSupplierResponse = $this->ijTenantApiRequest(
            method: 'GET',
            route: $this->resourceBaseName . ".read",
            routeParams: $params,
            data: $minimalSupplierDto,
        );

        if($this->showDebugOutput === true) {
            echo "\n\n";
            print_r($readSupplierResponse->json());
        }

        $readSupplierResponse->assertStatus(200)
            ->assertJsonStructure([])
            ->assertJson(fn (AssertableJson $json) =>
            $json->has('uuid')
                ->etc()
            );

    }

    public function test_basic_updates_with_a_minimal_supplier()
    {
        $expectedSupplierName = 'Schöner Baum Hotel';
        $minimalSupplierDto = array(
            'name' => $expectedSupplierName,
        );

        $createSupplierResponse = $this->ijTenantApiRequest(
            method: 'POST',
            route: $this->resourceBaseName . ".create",
            data: $minimalSupplierDto,
        );

        if($this->showDebugOutput === true) {
            echo "\n\n";
            print_r($createSupplierResponse->json());
        }

        $createSupplierResponse->assertStatus(200)
            ->assertJsonStructure([])
            ->assertJson(fn (AssertableJson $json) =>
            $json->has('uuid')
                ->etc()
            );

        $createdSuppplierJSON =  $createSupplierResponse->json();
        $updatedSupplierName = 'Grüner Baum Hotel';
        $params = array(
            'uuid' => $createdSuppplierJSON['uuid'],
            'name' => $updatedSupplierName,
        );
        $updates = &$params;
        $updatedSupplierResponse = $this->ijTenantApiRequest(
            method: 'PUT',
            route: $this->resourceBaseName . ".update",
            routeParams: $params,
            data: $updates,
        );

        if($this->showDebugOutput === true) {
            echo "\n\n";
            print_r($updatedSupplierResponse->json());
        }

        $updatedSupplierResponse->assertStatus(200)
            ->assertJsonStructure([])
            ->assertJson(fn (AssertableJson $json) =>
            $json->where('uuid', $createdSuppplierJSON['uuid'])
                ->where('name', $updatedSupplierName)
                ->etc()
            );
    }

    public function test_basic_deletes_of_a_minimal_supplier() {
        $expectedSupplierName = 'Großer Baum Hotel Spa';
        $minimalSupplierDto = array(
            'name' => $expectedSupplierName,
        );
        $createSupplierResponse = $this->ijTenantApiRequest(
            method: 'POST',
            route: $this->resourceBaseName . ".create",
            data: $minimalSupplierDto,
        );

        $createdSupplierJSON =  $createSupplierResponse->json();
        if($this->showDebugOutput === true) {
            echo "\n\nCreation response is:\n";
            print_r($createdSupplierJSON);
        }

        $createSupplierResponse->assertStatus(200)
            ->assertJsonStructure([])
            ->assertJson(fn (AssertableJson $json) =>
            $json->where('uuid', $createdSupplierJSON['uuid'])
                ->etc()
            );

        // delete created supplier

        $params = array(
            'uuid' => $createdSupplierJSON['uuid'],
        );
        $deleteSupplierResponse = $this->ijTenantApiRequest(
            method: 'DELETE',
            route: $this->resourceBaseName . ".delete",
            routeParams: $params,
        );

        $deleteSupplierResponseJSON = $deleteSupplierResponse->json();
        if($this->showDebugOutput === true) {
            echo "\n\nDeletion response is:\n";
            print_r($deleteSupplierResponseJSON);
        }

        $deleteSupplierResponse->assertStatus(200)
            ->assertJson(fn (AssertableJson $assertableJSON) =>
                $this->assertApiMessageDeletedFormat(
                    assertableJson: $assertableJSON,
                    resourceUUID: $deleteSupplierResponseJSON['uuid'])
            );

        // ensure supplier is really deleted supplier, expect 404
        $params = array(
            'uuid' => $createSupplierResponse->json()['uuid'],
        );
        $failedReadSupplierResponse = $this->ijTenantApiRequest(
            method: 'GET',
            route: $this->resourceBaseName . ".read",
            routeParams: $params,
        );

        $failedReadSupplierResponseJSON = $failedReadSupplierResponse->json();
        if($this->showDebugOutput === true) {
            echo "\n\nFailed read response is:\n";
            print_r($failedReadSupplierResponseJSON);
        }
        $failedReadSupplierResponse->assertStatus(404);
    }

    public function todo_test_external_updates_adminstation_fields_not_allowed() {
        // forbiden/fixed like:
        // - uuid
        // - timestamps
        // - kode
        // ...
        $this->assertTrue(false);
    }

    public function todo_test_deleting_a_not_existing_supplier() {
        // to be defined in detail.
        // response with 404 anyways.
        // {uuid: xxxx, state: 'unchanged', reason: ""}
        $this->assertTrue(false);
    }

    public function todo_test_reading_a_not_existing_supplier() {
        // to be defined in detail.
        // response with 404 anyways.
        // {uuid: xxxx, state: 'not found', reason: ""}
        $this->assertTrue(false);
    }

    public function test_create_a_supplier_having_address_data()
    {
        $expectedSupplierName = 'Hotel Unter den Linden';
        $supplierCreationRequestDTO = [
            'name' => $expectedSupplierName,
            'address' => [
                Location::FIELD_ADDRESSLINE_FIRST => "Hauptstrasse 1",
                Location::FIELD_CITY => "Berlin",
                Location::FIELD_ZIPCODE => "45123",
                Location::FIELD_COUNTRY => "Germany",
            ],
        ];

        $response = $this->ijTenantApiRequest(
            method: 'POST',
            route: $this->resourceBaseName . ".create",
            data: $supplierCreationRequestDTO,
        );

        if($this->showDebugOutput === true) {
            echo "\n\n";
            print_r($response->content());
        }

        $response->assertStatus(200)
            ->assertJsonStructure([])
            ->assertJson(fn (AssertableJson $json) =>
            $json->has('uuid')
                ->has('kode')
                ->where('name', $expectedSupplierName)
                ->has('address')
                ->where(
                    "address." . Location::FIELD_ADDRESSLINE_FIRST,
                    $supplierCreationRequestDTO['address'][Location::FIELD_ADDRESSLINE_FIRST]
                )
                ->where(
                    "address." . Location::FIELD_CITY,
                    $supplierCreationRequestDTO['address'][Location::FIELD_CITY]
                )
                ->where(
                    "address." . Location::FIELD_ZIPCODE,
                    $supplierCreationRequestDTO['address'][Location::FIELD_ZIPCODE]
                )
                ->where(
                    "address." . Location::FIELD_COUNTRY,
                    $supplierCreationRequestDTO['address'][Location::FIELD_COUNTRY]
                )
                ->where("address." . Location::FIELD_ADDRESSLINE_THIRD, null)
                ->etc()
            );
    }

    public function todo_test_updating_a_supplier_having_address_data() {
        $this->assertTrue(false);
    }

    public function todo_test_exception_on_supplier_with_duplicate_kode() {
        $this->assertTrue(false);
    }

    public function todo_test_exception_on_supplier_with_incomplete_fields() {
        $this->assertTrue(false);
    }


}
