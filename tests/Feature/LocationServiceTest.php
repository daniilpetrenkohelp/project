<?php

namespace Tests\Feature;

use App\Domain\Contacts\Models\Entities\Location;
use App\Domain\Contacts\Services\LocationService;
use App\Domain\Core\Models\Contact;
use App\Exceptions\DomainObjectInvalidException;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

class LocationServiceTest extends TestCase
{
    /**
     * @var LocationService
     */
    private mixed $locationService;

    function setUp(): void
    {
        parent::setUp();
        $this->locationService = $this->app->make('App\Domain\Contacts\Services\LocationService');

        DB::beginTransaction();
    }

    function tearDown(): void
    {
        DB::rollback();
        parent::tearDown();
    }

    function test_location_service_Createable() {
        $this->assertTrue($this->locationService !== null);
    }

    function test_creation_stamps_exist() {
        $addressDTO = array(
            'zipcode' => '45138',
        );

        tenancy()->runForMultiple([$this->currentTenant], function() use ($addressDTO) {
            auth()->setUser($this->currentUser);
            $addressLocationEntity = Location::create($addressDTO);
            $this->assertNotNull($addressLocationEntity);
            $this->assertNotNull($addressLocationEntity->created_by);
            $this->assertEquals($addressLocationEntity->created_by, $this->currentUser->id);
            $this->assertNotNull($addressLocationEntity->created_at);
            $this->assertNull($addressLocationEntity->updated_by);
        });
    }

    function test_update_stamps_exist() {
        $addressDTO = array(
            'zipcode' => '45138',
        );

        tenancy()->runForMultiple([$this->currentTenant], function() use ($addressDTO) {
            auth()->setUser($this->currentUser);
            $addressLocationEntity = Location::create($addressDTO);
            $this->assertNull($addressLocationEntity->updated_at);

            $updatedAddressLocationEntity = Location::where('id', $addressLocationEntity->id)->firstOrFail();
            $updatedAddressLocationEntity->zipcode = '44319';
            sleep(1);
            $saved = $updatedAddressLocationEntity->save();

            if($this->showDebugOutput) {
                echo "\n\nLocation after updating: \n\n";
                print(json_encode($updatedAddressLocationEntity));
            }

            $this->assertTrue($saved);
            $this->assertNotEquals($addressLocationEntity->zipcode, $updatedAddressLocationEntity->zipcode);
            $this->assertNotEquals($addressLocationEntity->updated_by, $updatedAddressLocationEntity->updated_by);
            $this->assertNotNull($updatedAddressLocationEntity->updated_at);

            $createdAt = $updatedAddressLocationEntity->created_at->toDateTimeString();
            $updatedAt = $updatedAddressLocationEntity->updated_at->toDateTimeString();
            $this->assertNotEquals($createdAt, $updatedAt);
        });
    }

    function test_create_location_with_minimal_address() {
        $addressDTO = [
            Location::FIELD_ADDRESSLINE_FIRST => 'Hauptstraße 12',
            Location::FIELD_ZIPCODE => '45138',
            Location::FIELD_CITY => 'Essen',
        ];

        tenancy()->runForMultiple([$this->currentTenant], function ($tenant) use ($addressDTO) {
            auth()->setUser($this->currentUser);
            $addressLocationEntity = $this->locationService->createByAddress($addressDTO);
            $this->assertNotNull($addressLocationEntity);
            $this->assertNotNull($addressLocationEntity->uuid);
            $this->assertEquals($addressLocationEntity->zipcode, $addressDTO['zipcode']);
        });
    }

    function test_deny_location_with_incomplete_address() {
        $addressDTO = [
        ];

        tenancy()->runForMultiple([$this->currentTenant], function() use ($addressDTO) {
            auth()->setUser($this->currentUser);
            try {
                $addressLocationEntity = $this->locationService->createByAddress($addressDTO);
                $this->assertNull($addressLocationEntity);
            } catch (DomainObjectInvalidException $error) {
                $this->assertTrue(true, 'This exception is intentional.');
            } catch (\Exception $error) {
                $this->assertTrue(false, 'Unexpected exception occured');
            }
        });
    }

    function test_create_location_with_complete_address() {
        $addressDTO = [
            Location::FIELD_ADDRESSLINE_FIRST => 'Ferienanlagen',
            Location::FIELD_ADDRESSLINE_SECOND => 'Hauptstrasse 12',
            Location::FIELD_ADDRESSLINE_THIRD => 'Hinterhaus',
            Location::FIELD_ZIPCODE => '45138',
            Location::FIELD_CITY => 'Berlin',
            Location::FIELD_PROVINCE => 'NRW',
            Location::FIELD_COUNTRY => 'Germany',
        ];

        tenancy()->runForMultiple([$this->currentTenant], function ($tenant) use ($addressDTO) {
            auth()->setUser($this->currentUser);
            $addressLocationEntity = $this->locationService->createByAddress($addressDTO);
            $this->assertNotNull($addressLocationEntity);
            $this->assertNotNull($addressLocationEntity->uuid);
            $this->assertEquals($addressLocationEntity->zipcode, $addressDTO['zipcode']);
        });
    }

    function todo_test_ensure_long_lat_locations_have_both_values_always() {
        $this->assertTrue(false);
    }
}
