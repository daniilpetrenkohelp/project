<?php

namespace Tests;

use App\Helpers\Http\APIEffectStates;
use Illuminate\Testing\Fluent\AssertableJson;

trait IJTestUtils {

    /**
     *
     * Validate the standard format of an error result.
     * Every error message JSON, received from the IJ API must comply to this format.
     *
     * @param AssertableJson $assertableJson
     * @param string $expectedOutcomeState
     * @param string|null $resourceUUID
     * @return AssertableJson
     */
    public function assertApiMessageFormat(
        AssertableJson &$assertableJson,
        string         $expectedOutcomeState = APIEffectStates::OUTCOME_STATE_UNCHANGED,
        string         $resourceUUID = null) : AssertableJson
    {
        $assertableJson
            ->where('uuid', $resourceUUID)
            ->where('state', $expectedOutcomeState);

        if($expectedOutcomeState != APIEffectStates::OUTCOME_STATE_DELETED) {
            $assertableJson->has('reason');
        }

        return $assertableJson;
    }

    public function assertApiMessageDeletedFormat(
        AssertableJson &$assertableJson,
        string $resourceUUID) : AssertableJson {
        $this->assertApiMessageFormat(
            assertableJson: $assertableJson,
            expectedOutcomeState: APIEffectStates::OUTCOME_STATE_DELETED,
            resourceUUID: $resourceUUID
        );

        return $assertableJson;
    }

    /**
     *
     * Validate the standard format of a listing result.
     * Every listing JSON, received from the IJ API must comply to this format.
     *
     * @param array $listResponseJSON
     * @param AssertableJson $assertableJson
     * @return AssertableJson
     *
     */
    public function assertApiResourceListingFormat(
        AssertableJson $assertableJson,
        array $listResponseJSON = null,
        mixed $listResponse = null) : AssertableJson
    {
        if($listResponse !== null) {
            $listResponseJSON = $listResponse->json();
        }
        return $assertableJson->has('size')
            ->has('data')
            ->where('size', count($listResponseJSON['data']))
            ->etc();
    }

    public function ijTenantApiRequest(
        string $method,
        string $route,
        array  $routeParams = [],
        array  $headers = [],
        array  $data = []) {

        $endpointUrl = $this->generateTenantApiRoute($route, $routeParams);

        $additionalHeaders = &$headers;
        $requiredHeaders = $this->generateRequiredHeaders();
        $mergedHeaders = array($requiredHeaders, $additionalHeaders);

        return $this->actingAs($this->currentUser)->json(
            method: $method,
            uri: $endpointUrl,
            data: $data,
            headers: $mergedHeaders,
        );
    }

    public function generateRequiredHeaders() {
        $apiToken = $this->currentUser->api_token;
        return [
            "Authorization" => "Bearer ${apiToken}",
        ];
    }

    protected function generateTenantApiRoute(string $routeName, array $params = array()):string
    {
        $tenantDomain = $this->currentTenant->domains->first()->domain;
        $urlPath = route(
            $routeName,
            $params,
            false,
        );
        return "http://${tenantDomain}${urlPath}";
    }

    public function responseDebugLog(
        string $message,
        mixed $response,
    ) {
        if($this->showDebugOutput === true) {
            echo "\n\n${message}\n";

            $jsonResponse = $response->json();

            $output = print_r($jsonResponse, true);
            $lines = explode(
                "\n",
                $output
            );
            $maxLines = env('IJ_TESTING_SHOW_DEBUG_MAX_LINES', 15);
            $lines = array_slice($lines, 0, $maxLines);
            $output = implode("\n", $lines);

            echo $output;
            echo "\n\n";
        }
    }

}
