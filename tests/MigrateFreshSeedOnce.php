<?php

namespace Tests;

use App\Domain\Core\Services\RegistrationService;
use App\Domain\Core\Services\TenantSetupService;
use Illuminate\Support\Facades\Artisan;

trait MigrateFreshSeedOnce {

    /**
     * If true, setup has run at least once.
     * @var boolean
     */
    protected static $setUpHasRunOnce = false;

    /**
     * After the first run of setUp "migrate:fresh --seed"
     * @return void
     */
    protected function initializeTestDatabase(): void
    {
        if (!static::$setUpHasRunOnce) {
            Artisan::call('ij:testing:db:wipe');
            Artisan::call('db:wipe');
            Artisan::call('migrate:fresh');

            $testingTenantEmail = env('IJ_TESTING_TENANT_EMAIL');
            $tenantSetupService = new TenantSetupService();
            $registrationService = new RegistrationService($tenantSetupService);
            $registrationService->setupByMailAndProvisionMinimal($testingTenantEmail);

            static::$setUpHasRunOnce = true;
        }
    }
}
