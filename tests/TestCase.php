<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use App\Domain\Core\Models\User;

/**
 * @property IJTestUtils ijTestUtils
 */
abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;
    use MigrateFreshSeedOnce;
    use IJTestUtils;

    /**
     * @var mixed
     */
    private $tenantService;
    /**
     * @var mixed
     */
    protected $currentTenant;
    /**
     * @var mixed
     */
    protected $currentUser;
    /**
     * @var boolean
     */
    protected $showDebugOutput;

    function setUp(): void
    {
        parent::setUp();

        $this->initializeTestDatabase();
        $this->initializeTestTenant();

        $this->showDebugOutput = env('IJ_TESTING_SHOW_DEBUG_OUTPUT', false);
    }

    private function initializeTestTenant() : void {
        $testingTenantEmail = env('IJ_TESTING_TENANT_EMAIL');
        $this->tenantService = $this->app->make('App\Domain\Core\Services\TenantService');
        $this->currentTenant = $this->tenantService->findByRegistrationEmail($testingTenantEmail);
        $saasTenant = $this->currentTenant;
        tenancy()->runForMultiple([$this->currentTenant->id], function ($tenant) use ($saasTenant) {
            $this->currentUser = User::where('email', $saasTenant->registration_email)->firstOrFail();
        });
    }

}
