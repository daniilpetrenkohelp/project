<?php

namespace Database\Helpers;

use Illuminate\Database\Schema\Blueprint;

trait IJMigration
{

    public function userStamps(Blueprint $table) {
        $table->unsignedBigInteger('created_by')->nullable();
        $table->foreign('created_by')
            ->references('id')
            ->on('users');

        $table->unsignedBigInteger('updated_by')->nullable();
        $table->foreign('updated_by')
            ->references('id')
            ->on('users');
    }

}
