<?php

namespace Database\Seeders;

use App\Domain\Core\Models\User;
use Illuminate\Database\Seeder;
use App\Domain\Contacts\Models\Entities\Contact;
use App\Domain\Contacts\Models\Entities\ContactDetail;
use App\Domain\Contacts\Models\Entities\FinancialDetail;
use App\Domain\Contacts\Models\Entities\Location;
use App\Domain\Contacts\Models\Entities\SupplierContact;
use App\Domain\Core\Models\Supplier;
use App\Domain\Sourcing\Models\Contract;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $suppliersAmount = 5;
        $contactsPerSupplier = 5;
        $contractsAmount = 5;

        $currentUser = User::where('id', 1)->firstOrFail();
        auth()->setUser($currentUser);

        $suppliers = Supplier::factory($suppliersAmount)
            ->create([
            ]);

        $contacts = Contact::factory($suppliersAmount * $contactsPerSupplier)
            ->create([
            ]);

        SupplierContact::factory(50)
            ->create([
            ])
            ->each(function ($supplierRole) use (&$suppliers, &$contacts) {
                $supplierRole->update(['supplier_id' => $suppliers->random()->id]);
                $supplierRole->update(['contact_id' => $contacts->random()->id]);
            });

        Location::factory($suppliersAmount * $contactsPerSupplier)
            ->create([
            ])
            ->each(function ($location) use ($contacts) {
                $location->update(['contact_id' => $contacts->random()->id]);
            });

        ContactDetail::factory($suppliersAmount * $contactsPerSupplier * 2)
            ->create([
            ])
            ->each(function ($contactDetail) use ($contacts) {
                $contactDetail->update(['contact_id' => $contacts->random()->id]);
            });

        FinancialDetail::factory(5)
            ->create([
            ])
            ->each(function ($contactDetail) use ($contacts) {
                $contactDetail->update(['contact_id' => $contacts->random()->id]);
            });

        Contract::factory($contractsAmount)
            ->create([
            ])
            ->each(function ($contract) use ($suppliers) {
                $contract->update(['supplier_id' => $suppliers->random()->id]);
            });

    }
}
