<?php

namespace Database\Factories;

use App\Domain\Sourcing\Models\Contract;
use Illuminate\Database\Eloquent\Factories\Factory;

class ContractFactory extends Factory
{

    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Contract::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $randomKode = explode('-', $this->faker->uuid);
        $randomKode = array_pop($randomKode);

        $revenuePaymentMethod = $this->faker->randomElement([
            Contract::REVENUE_PAYMENT_INVOICE,
            Contract::REVENUE_PAYMENT_OTHER,
        ]);

        $pricingFormat = $this->faker->randomElement([
            Contract::PRICING_NET,
            Contract::PRICING_GROSS,
        ]);

        return [
            'uuid' => $this->faker->uuid,
            'total_edits_amount' => 0,
            'kode' => $randomKode,
            'revenue_share_percent' => $this->faker->randomNumber(5),
            'revenue_payment_method' => $revenuePaymentMethod,
            'pricing_format' => $pricingFormat,
        ];
    }

}
