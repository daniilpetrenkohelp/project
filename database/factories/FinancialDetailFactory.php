<?php

namespace Database\Factories;

use App\Domain\Contacts\Models\Entities\FinancialDetail;
use Illuminate\Database\Eloquent\Factories\Factory;

class FinancialDetailFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = FinancialDetail::class;


    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $invoiceType = $this->faker->randomElement([
            FinancialDetail::INVOICE_FORMAT_EMAIL,
            FinancialDetail::INVOICE_FORMAT_PAPER,
            FinancialDetail::INVOICE_FORMAT_PDF,
            FinancialDetail::INVOICE_FORMAT_ITALYMAIL,
        ]);

        return [
            'uuid' => $this->faker->uuid,
            'banking_institute' => $this->faker->company,
            'iban' => $this->faker->iban('IT'),
            'swift' => $this->faker->swiftBicNumber,
            'bic' => $this->faker->swiftBicNumber,
            'invoice_format' => $invoiceType,
        ];
    }
}
