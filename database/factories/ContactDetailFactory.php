<?php

namespace Database\Factories;

use App\Domain\Contacts\Models\Entities\ContactDetail;
use Illuminate\Database\Eloquent\Factories\Factory;

class ContactDetailFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ContactDetail::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $type = $this->faker->randomElement([
            ContactDetail::TYPE_PHONE,
            ContactDetail::TYPE_EMAIL,
            ContactDetail::TYPE_FACEBOOK,
            ContactDetail::TYPE_FAX,
            ContactDetail::TYPE_WEBSITE,
            ContactDetail::TYPE_MOBILE,
        ]);

        $value = null;
        if (in_array($type, [ContactDetail::TYPE_MOBILE, ContactDetail::TYPE_PHONE, ContactDetail::TYPE_FAX])) {
            $value = $this->faker->phoneNumber;
        } else if (in_array($type, [ContactDetail::TYPE_EMAIL])) {
            $value = $this->faker->email;
        } else if (in_array($type, [ContactDetail::TYPE_WEBSITE, ContactDetail::TYPE_FACEBOOK])) {
            $value = $this->faker->url;
        }

        return [
            'uuid' => $this->faker->uuid,
            'type' => $type,
            'value' => $value,
            'is_primary' => false,
        ];
    }
}
