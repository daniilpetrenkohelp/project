<?php

namespace Database\Factories;

use App\Domain\Contacts\Models\Entities\Location;
use Illuminate\Database\Eloquent\Factories\Factory;

class LocationFactory extends Factory
{

    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Location::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'uuid' => $this->faker->uuid,
            Location::FIELD_ADDRESSLINE_FIRST => $this->faker->streetAddress,
            Location::FIELD_ADDRESSLINE_SECOND => $this->faker->streetSuffix,
            Location::FIELD_ZIPCODE => $this->faker->postcode,
            Location::FIELD_CITY => $this->faker->city,
            Location::FIELD_PROVINCE => null,
            Location::FIELD_COUNTRY => $this->faker->country,
            Location::FIELD_GPS_LATITUDE => null,
            Location::FIELD_GPS_LONGITUDE => null,
        ];
    }

}
