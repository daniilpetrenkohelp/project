<?php

namespace Database\Factories;

use App\Domain\Contacts\Models\Entities\SupplierContact;
use Illuminate\Database\Eloquent\Factories\Factory;

class SupplierContactFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = SupplierContact::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $role = $this->faker->randomElement(SupplierContact::ALL_CONTRACTABLE_ROLES);
        $roles = [$role];

        return [
            'uuid' => $this->faker->uuid,
            'roles' => $roles,
            'is_primary' => false,
        ];
    }
}
