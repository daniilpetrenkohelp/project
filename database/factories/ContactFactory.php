<?php

namespace Database\Factories;

use App\Domain\Contacts\Models\Entities\Contact;
use Illuminate\Database\Eloquent\Factories\Factory;

class ContactFactory extends Factory
{

    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Contact::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $type = $this->faker->randomElement([
            Contact::TYPE_PERSON,
            Contact::TYPE_COMPANY,
            Contact::TYPE_ORGANIZATION,
        ]);

        if($type === Contact::TYPE_PERSON) {
            return [
                'uuid' => $this->faker->uuid,
                'type' => $type,
                'name' => $this->faker->firstName ." ". $this->faker->lastName,
            ];
        } else {
            return [
                'uuid' => $this->faker->uuid,
                'type' => $type,
                'name' => $this->faker->company,
            ];
        }
    }

}
