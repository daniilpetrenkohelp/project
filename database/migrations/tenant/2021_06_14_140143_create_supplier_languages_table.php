<?php

use Database\Helpers\IJMigration;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSupplierLanguagesTable extends Migration
{
    use IJMigration;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supplier_languages', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid');

            $table->boolean('is_primary')->default(false);
            $table->unsignedBigInteger('supplier_id')->nullable();
            $table->foreign('supplier_id')
                ->nullable()
                ->references('id')
                ->on('suppliers');

            $table->unsignedBigInteger('language_id')->nullable();
            $table->foreign('language_id')
                ->nullable()
                ->references('id')
                ->on('languages');

            $table->timestampsTz();
            $this->userStamps($table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supplier_languages');
    }
}
