<?php

use App\Domain\Contacts\Models\Entities\SupplierContact;
use Database\Helpers\IJMigration;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSupplierContactsTable extends Migration
{
    use IJMigration;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supplier_contacts', function (Blueprint $table) {
            $table->id('id');
            $table->uuid('uuid');

            $table->unsignedBigInteger('supplier_id')->nullable();
            $table->foreign('supplier_id')
                ->nullable()
                ->references('id')
                ->on('suppliers');

            $table->unsignedBigInteger('contact_id')->nullable();
            $table->foreign('contact_id')
                ->nullable()
                ->references('id')
                ->on('contacts');

            $table->json('roles');
            $table->boolean('is_primary')->default(false);

            $table->timestampsTz();
            $table->softDeletesTz();
            $this->userStamps($table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supplier_contacts');
    }
}
