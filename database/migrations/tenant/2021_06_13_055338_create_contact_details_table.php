<?php

use App\Domain\Contacts\Models\Entities\ContactDetail;
use Database\Helpers\IJMigration;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContactDetailsTable extends Migration
{
    use IJMigration;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contact_details', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid');

            $table->unsignedBigInteger('contact_id')->unsigned()->nullable();
            $table->foreign('contact_id')
                ->references('id')
                ->on('contacts');

            $table->enum('type', ContactDetail::ALL_CHANNEL_TYPES);

            $table->boolean('is_primary')->default(false);
            $table->string('value');

            $table->timestampsTz();
            $table->softDeletesTz();

            $this->userStamps($table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contact_details');
    }
}
