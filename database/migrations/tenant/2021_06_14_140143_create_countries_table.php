<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('countries', function (Blueprint $table) {
            $table->id();

            $table->integer("zone_flag");
            $table->boolean("is_uno_member");
            $table->string("internet_tld", 64);
            $table->string("shortname_local");
            $table->string("shortname_en");
            $table->string("shortname_de");
            $table->string("calling_code");
            $table->string("official_name_local");
            $table->string("official_name_en");
            $table->string("official_name_de");
            $table->integer("iso_nr");
            $table->string("iso_code");
            $table->string("short_id");
            $table->boolean("is_eu_member")->default(false);
            $table->integer("currency_iso_number");
            $table->string("currency_iso_code");
            $table->string("capital");
            $table->integer("address_format");

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('countries');
    }
}
