<?php

use Database\Helpers\IJMigration;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFinancialDetailsTable extends Migration
{
    use IJMigration;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('financial_details', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid');

            $table->unsignedBigInteger('contact_id')->unsigned()->nullable();
            $table->foreign('contact_id')
                ->references('id')
                ->on('contacts');

            $table->string('banking_institute');
            $table->string('bic');
            $table->string('iban');
            $table->string('swift')->nullable();
            $table->string('vat_ident')->nullable();
            $table->string('tax_ident')->nullable();
            $table->enum('invoice_format', [
                'italymail',
                'pdf',
                'email',
                'paper',
            ])->default('pdf');
            $table->string('invoice_language')->nullable();

            $table->timestampsTz();
            $table->softDeletesTz();

            $this->userStamps($table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('financial_details');
    }
}
