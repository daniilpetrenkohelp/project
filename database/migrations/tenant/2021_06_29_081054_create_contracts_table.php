<?php

use Database\Helpers\IJMigration;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Domain\Sourcing\Models\Contract;

class CreateContractsTable extends Migration
{
    use IJMigration;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contracts', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid');

            $table->unsignedBigInteger('supplier_id')->nullable();
            $table->foreign('supplier_id')
                ->nullable()
                ->references('id')
                ->on('suppliers');

            $table->string('version_fingerprint')->nullable();
            $table->integer('total_edits_amount')->default(0);
            $table->string('kode')->nullable();
            $table->integer('revenue_share_percent')->default(0);
            $table->enum('revenue_payment_method', [
                'invoice',
                'other',
            ])->default(Contract::REVENUE_PAYMENT_INVOICE);
            $table->enum('pricing_format', [
                'net',
                'gross',
            ])->default(Contract::PRICING_NET);
            $table->json('document')->nullable();

            $table->timestampsTz();
            $table->softDeletesTz();

            $this->userStamps($table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contracts');
    }
}
