<?php

use App\Domain\Contacts\Models\Entities\Location;
use Database\Helpers\IJMigration;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLocationsTable extends Migration
{
    use IJMigration;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('locations', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid');

            $table->unsignedBigInteger('supplier_id')->nullable();
            $table->foreign('supplier_id')
                ->references('id')
                ->on('suppliers');

            $table->unsignedBigInteger('contact_id')->nullable();
            $table->foreign('contact_id')
                ->nullable()
                ->references('id')
                ->on('contacts');

            $table->string(Location::FIELD_ADDRESSLINE_FIRST)->nullable();
            $table->string(Location::FIELD_ADDRESSLINE_SECOND)->nullable();
            $table->string(Location::FIELD_ADDRESSLINE_THIRD)->nullable();
            $table->string(Location::FIELD_ZIPCODE)->nullable();
            $table->string(Location::FIELD_CITY)->nullable();
            $table->string(Location::FIELD_PROVINCE)->nullable();
            $table->string(Location::FIELD_COUNTRY)->nullable();
            $table->string(Location::FIELD_GPS_LATITUDE)->nullable();
            $table->string(Location::FIELD_GPS_LONGITUDE)->nullable();

            $table->timestampsTz();
            $table->softDeletesTz();
            $this->userStamps($table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('locations');
    }
}
