import * as moment from "moment"

function generateSourcingContractSkeleton() {
    return {
        _meta: {
            type: "sourcing",
            version: "0.0.0",
            requires: [
                { type: "plugin", identifier: "italy-pec" },
            ],
        },
        kode: "123123123",
        contract_partner: {
            supplier_uuid: "xxxx-xxxxx-xxxxx-xxxx",
            supplier_contact_uuid: "xxxx-xxxxx-xxxxx-xxxx",
            address: {
                name: "Meier",
                address: "Hauptstrasse 4",
                zipcode: "45138",
                city: "Anywhere",
                contry_iso_code: "DE",
            },
        },
        contacts: [
            {
                uuid: "123-123-123-123",
                isOrganisation: false,
                familyName: "Mustermann",
                givenName: "Max",
                category: "Ansprechpartner",
                role: "Inhaber",
                email: "max@mail.de",
                activeSince: moment().toISOString(),
                activeUntil: null,
            },
            {
                uuid: "456-456-456-456",
                isOrganisation: false,
                familyName: "Mustermann",
                givenName: "Melissa",
                category: "Ansprechpartner",
                role: "",
                email: "melissa@mail.de",
                activeSince: null,
                activeUntil: null,
            },
            {
                uuid: "101-101-101-101",
                isOrganisation: false,
                familyName: "Mustermann-Mannmuster",
                givenName: "Mareike",
                category: "Ansprechpartner",
                role: "Inhaber",
                email: "mareike@mail.de",
                activeSince: moment().toISOString(),
                activeUntil: moment().toISOString(),
            },
        ],
        invoiceDetails: {
            useContractAddressAsInvoiceAddress: true,
            invoiceContacts: [
                {
                    uuid: "789-789-789-789",
                    isOrganisation: true,
                    companyName: 'Hilton GmbH',
                    street: 'Hiltonstraße 12',
                    zipcode: '76829',
                    city: 'Köln',
                    countryIsoCode: 'DE',
                    vatId: "DE 1234567",
                },
            ],
        },
        bankDetails: [
            {
                iban: "DE XX XX XXXXXXXXXX",
            },
        ],
    }
}

export class ContractBuilder {

    constructor(originalContract) {
        this.originalContract = originalContract
        this.contract = generateSourcingContractSkeleton()
    }

    setSupplierAddress(address) {
        this.contract.address = address
    }

    setSupplierUUID(supplierUUID) {
        this.contract.contract_partner.supplier_uuid = supplierUUID
    }

    setSupplierContactUUID(supplierContactUUID) {
        this.contract.contract_partner.supplier_contact_uuid = supplierContactUUID
    }

    addContact(anotherContact) {
        const contacts = this.contract.contacts

        for(const idx in contacts) {
            // to avoid duplicates
            if(contacts[idx] == anotherContact) {
                return
            }
        }
        contacts.push(anotherContact)
    }

    removeContact(anotherContact) {
        const contacts = this.contract.contacts
        let idx = -1
        for(const pos in contacts) {
            if(contacts[pos]._internal == anotherContact._internal) {
                idx = pos
                break
            }
        }

        if(idx !== -1) {
            contacts.splice(idx, 1)
        }
    }

    generateCopy() {
        console.log('a')
        let a = JSON.stringify(this.contract)
        console.log('b')
        a = JSON.parse(a)
        console.log('c', a)
        return a
    }

}
