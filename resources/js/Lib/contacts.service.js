export default {

    readAllContacts: ($inertia) => {
        let listContactsUrl = $inertia.route('api.contacts').hydrateUrl()
        return fetch(listContactsUrl, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
        })
            .then((response) => {
                return response.json()
            })
            .then((data) => {
                const contacts = data
                return contacts
            })
    },

}
