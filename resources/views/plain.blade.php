<!DOCTYPE html>
<html lang="en">
<head>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Meta -->
    <meta name="description" content="Responsive Bootstrap 4 Dashboard Template">
    <meta name="author" content="IncomingJET">

    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="../../assets/img/favicon.png">

    <title>DashForge Responsive Bootstrap 4 Dashboard Template</title>

    <!-- vendor css -->
    <link href="{{ global_asset('theme/lib/@fortawesome/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
    <link href="{{ global_asset('theme/lib/ionicons/css/ionicons.min.css') }}" rel="stylesheet">
    <link href="{{ global_asset('theme/lib/typicons.font/typicons.css') }}" rel="stylesheet">
    <link href="{{ global_asset('css/app.css') }}" rel="stylesheet">

    <!-- DashForge CSS -->
    <link rel="stylesheet" href="{{ global_asset('theme/assets/css/dashforge.css') }}">
    <link rel="stylesheet" href="{{ global_asset('theme/assets/css/dashforge.auth.css') }}">
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css" />
    <link rel="stylesheet" href="{{ global_asset('theme/lib/select2/css/select2.min.css') }}" />
    <link rel="stylesheet" href="{{ global_asset('theme/lib/jquery-simple-datetimepicker/jquery.simple-dtpicker.css') }}" />

    {{-- Inertia --}}
    <script src="https://polyfill.io/v3/polyfill.min.js?features=smoothscroll,NodeList.prototype.forEach,Promise,Object.values,Object.assign" defer></script>

    {{-- Ping CRM --}}
    <script src="https://polyfill.io/v3/polyfill.min.js?features=String.prototype.startsWith" defer></script>

    <script src="{{ mix('/js/app.js') }}" defer></script>
    @routes
</head>
<body>

@inertia

<script src="{{ global_asset('theme/lib/jquery/jquery.min.js') }}"></script>
<script src="{{ global_asset('theme/lib/jqueryui/jquery-ui.min.js') }}"></script>
<script src="{{ global_asset('theme/lib/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ global_asset('theme/lib/feather-icons/feather.min.js') }}"></script>
<script src="{{ global_asset('theme/lib/perfect-scrollbar/perfect-scrollbar.min.js') }}"></script>
<script src="//cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
<script src="{{ global_asset('theme/lib/jquery-simple-datetimepicker/jquery.simple-dtpicker.js') }}"></script>

<script src="{{ global_asset('theme/lib/select2/js/select2.full.min.js') }}"></script>
<script src="{{ global_asset('theme/assets/js/dashforge.js') }}"></script>

<!-- append theme customizer -->
<script src="{{ global_asset('theme/lib/js-cookie/js.cookie.js') }}"></script>
<script src="{{ global_asset('theme/assets/js/dashforge.settings.js') }}"></script>

<script>
    // $(function(){
    //     'use script'
    //
    //     window.darkMode = function(){
    //         $('.btn-white').addClass('btn-dark').removeClass('btn-white');
    //     }
    //
    //     window.lightMode = function() {
    //         $('.btn-dark').addClass('btn-white').removeClass('btn-dark');
    //     }
    //
    //     var hasMode = Cookies.get('df-mode');
    //     if(hasMode === 'dark') {
    //         darkMode();
    //     } else {
    //         lightMode();
    //     }
    // })
</script>
</body>
</html>
