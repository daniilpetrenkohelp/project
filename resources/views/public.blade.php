<!DOCTYPE html>
<html lang="en">
<head>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Meta -->
    <meta name="description" content="Responsive Bootstrap 4 Dashboard Template">
    <meta name="author" content="IncomingJET">

    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="../../assets/img/favicon.png">

    <title>DashForge Responsive Bootstrap 4 Dashboard Template</title>

    <!-- vendor css -->
    <link href="{{ global_asset('theme/lib/@fortawesome/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
    <link href="{{ global_asset('theme/lib/ionicons/css/ionicons.min.css') }}" rel="stylesheet">
    <link href="{{ global_asset('css/app.css') }}" rel="stylesheet">

    <!-- DashForge CSS -->
    <link rel="stylesheet" href="{{ global_asset('theme/assets/css/dashforge.css') }}">
    <link rel="stylesheet" href="{{ global_asset('theme/assets/css/dashforge.auth.css') }}">

    {{-- Inertia --}}
    <script src="https://polyfill.io/v3/polyfill.min.js?features=smoothscroll,NodeList.prototype.forEach,Promise,Object.values,Object.assign" defer></script>

    {{-- Ping CRM --}}
    <script src="https://polyfill.io/v3/polyfill.min.js?features=String.prototype.startsWith" defer></script>

    <script src="{{ mix('/js/app.js') }}" defer></script>
    @routes
</head>
<body>

<header class="navbar navbar-header navbar-header-fixed">
    <a href="" id="mainMenuOpen" class="burger-menu"><i data-feather="menu"></i></a>
{{--    <div class="navbar-brand">--}}
{{--    </div><!-- navbar-brand -->--}}
    <div id="navbarMenu" class="navbar-menu-wrapper">
        <span class="ijlogo">
            <svg width="100%" height="100%" viewBox="0 0 1185 266" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" xmlns:serif="http://www.serif.com/" style="fill-rule:evenodd;clip-rule:evenodd;stroke-linejoin:round;stroke-miterlimit:2;">
                <g transform="matrix(1.04107,0,0,1.04107,-42.3625,128.612)">
                    <g id="Layer1">
                        <g>
                            <text x="41.083px" y="55.006px" style="font-family:'DINOT', 'DIN OT', sans-serif;font-size:165px;">INC<tspan x="311.848px " y="55.006px ">O</tspan>MING</text>
                            <text x="828.298px" y="55.006px" style="font-family:'Arial-Black', 'Arial', sans-serif;font-weight:900;font-size:165px;">JET</text>
                        </g>
                    </g>
                </g>
            </svg>
        </span>

        <ul class="nav navbar-menu">
            <li class="nav-label pd-l-20 pd-lg-l-25 d-lg-none">Main Navigation</li>
            <li class="nav-item with-sub">
                <a href="" class="nav-link"><i data-feather="pie-chart"></i> Über</a>
                <ul class="navbar-menu-sub">
                    <li class="nav-sub-item"><a href="dashboard-one.html" class="nav-sub-link"><i data-feather="bar-chart-2"></i>Lösungen</a></li>
                    <li class="nav-sub-item"><a href="dashboard-two.html" class="nav-sub-link"><i data-feather="bar-chart-2"></i>Firma</a></li>
                </ul>
            </li>
            <li class="nav-item"><a href="../../collections/" class="nav-link"><i data-feather="archive"></i> Kontakt</a></li>
            <li class="nav-item"><a href="../../components/" class="nav-link"><i data-feather="box"></i> Impressum</a></li>
        </ul>
    </div><!-- navbar-menu-wrapper -->
    <div class="navbar-right">
        <a href="/" class="btn btn-buy">
            <i data-feather="user-plus"></i>
            <span>Jetzt registrieren</span>
        </a>
    </div><!-- navbar-right -->
</header><!-- navbar -->

@inertia

<footer class="footer">
    <div>
        <span>&copy; 2019 DashForge v1.0.0. </span>
        <span>Created by <a href="http://themepixels.me">ThemePixels</a></span>
    </div>
    <div>
        <nav class="nav">
            <a href="https://themeforest.net/licenses/standard" class="nav-link">Licenses</a>
            <a href="../../change-log.html" class="nav-link">Change Log</a>
            <a href="https://discordapp.com/invite/RYqkVuw" class="nav-link">Get Help</a>
        </nav>
    </div>
</footer>

<script src="{{ global_asset('theme/lib/jquery/jquery.min.js') }}"></script>
<script src="{{ global_asset('theme/lib/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ global_asset('theme/lib/feather-icons/feather.min.js') }}"></script>
<script src="{{ global_asset('theme/lib/perfect-scrollbar/perfect-scrollbar.min.js') }}"></script>

<script src="{{ global_asset('theme/assets/js/dashforge.js') }}"></script>

<!-- append theme customizer -->
<script src="{{ global_asset('theme/lib/js-cookie/js.cookie.js') }}"></script>
<script src="{{ global_asset('theme/assets/js/dashforge.settings.js') }}"></script>

<script>
    $(function(){
        'use script'

        window.darkMode = function(){
            $('.btn-white').addClass('btn-dark').removeClass('btn-white');
        }

        window.lightMode = function() {
            $('.btn-dark').addClass('btn-white').removeClass('btn-dark');
        }

        var hasMode = Cookies.get('df-mode');
        if(hasMode === 'dark') {
            darkMode();
        } else {
            lightMode();
        }
    })
</script>
</body>
</html>
